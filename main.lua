-- Surfer - main.lua

-- Dependencies:
---- Nothing

local SUBMOD = false
local SUBMODROOT = "surfer/"
local RESTREFILE = "libraries/restre"
if not RESTRE_DIR then
	if SUBMOD then
		RESTRE_DIR = SUBMODROOT
		require(RESTRE_DIR .. RESTREFILE)()
	else
		require(RESTREFILE)()
	end
end

if not CycloneLib then
	restre_require("libraries/cyclonelib/main")
end

restre_require("contents/survivors/survivors")
restre_require("contents/artifacts/artifacts")
restre_require("contents/items/items")
restre_require("contents/elites/elites")
restre_require("contents/difficulties/difficulties")
restre_require("contents/drones/drones")