# Surfer

Surfer is a mod for [Risk of Rain Modloader](https://rainfusion.ml) that adds some additional content to the game.  
The latest version can be found on the repository (https://gitlab.com/noneNA/surfer) under the `develop` branch.  
The changelog can be found in `CHANGELOG.md`  

## Includes

### Survivors

* Surfer: A reckless survivor that shines when outnumbered.
* Nyst: A survivor capable of stable damage output and long but painful invincibility frames.

### Items

* Exochip: Slightly increases your armor based on your current armor.  
* Jelly Root: Increases your speed the closer you are to enemies.  
* Leeching Seedling: Your attacks heal you based on the damage they deal.  
* Miniature Star: Nearby enemies are weakened to your next attack the longer they stand near you.  
* Night's Guidance: Takes you to the teleporter.  
* Ripple Rift: Makes an area that your attacks ripple in and deal damage to all enemies inside.  
* Shades: Chance to block half of the damage received.  
* Silent Siren: Enemies around you move and attack slower.  
* Static Reload: Periodically increases the damage of your attacks.  
* Tide's Promise: Blocks a flat amount of damage from each attack.  

### Difficulties

* Drain: A difficulty that aims to make the game more straightforward. Removes map objects and instead gives items at the end of teleporter events.  

### Elites

* Wrecked: Elites that while not particularly dangerous themselves might make other enemies so.  

### Artifacts

* Fusion: Chance to fuse three items of a lower tier to a higer tier on item pickup. (Needs the 'Item Removal Library' dependency)  
* Magnet: Items are attracted towards you.  
* Rain: Now it certainly rains.  

### Drones

* Graw: A drone that shares an identical copy of your items. (Needs the 'Global Items' dependency)  

## Contact

You can contact me through email `nonena@protonmail.com` or through the discord under the name `none#3549`.  