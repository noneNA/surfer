# Surfer Changelog

## Surfer 0.2.4
* Graw (new drone)
* Nyst's Gather now stacks diminishingly
* Nyst's Avalanche now stuns and knocks back
* Tweaked Surfer's wave movements
* Removed Surfer's drying mechanic
* Fixed Nyst's Snowman not scaling with speed
* Fixed bug due to outdated CycloneLib version

## Surfer 0.2.3
* Siphon artifact
* Wrecked Magma Worm
* Fixed Drain item banning code
* GlobalItems added as optional dependency for Siphon
* Fixed teleporter indicator text being active when Drain is not

## Surfer 0.2.2:
* Shorter item descriptions and pickup text
* Fixed weird Silent Siren bug
* Fixed Drain item error on singleplayer
* Magma Worms can't be Wrecked anymore
* Reworked the Rain artifact

## Surfer 0.2.1:
* Added basic multiplayer support
* Increased Exochip armor from 10% to 20%

## Surfer 0.2:
* Drain difficulty
* Nyst survivor
* Wrecked elites
* Nine new items
* Complete surfer rework
* Some code re-written
* Project structure rework

## Surfer 0.1.13
* Change : Added cooldown to overcharge.
* Change : Increased the life of II-Charge+Discharge.
* Trivial : Moved to CycloneLib

## Surfer 0.1.12
* Fix : Fixed Fusion initializing without itemremover.

## Surfer 0.1.11
* Change : The charging animation now reflects the cooldown.
* Change : Disabled two artifacts since they can't be properly implemented.
* Change : Updated the ForceBuff Library to match the new version.
* Fix : I-Charge+Discharge not attacking the correct direction when charging for small amounts.
* Trivial : Removed moveDown that was there for some reason.

## Surfer 0.1.10
* Fix : Turned off debug mode.
* Fix : Animation direction being locked when not successfully discharging.

## Surfer 0.1.9
* Change : Lowered the wind effect on Rain
* Change : Changed how I-Charge+Discharge works. (Increased chainability, changed damage formula)
* Change : I-Charge+Discharge also charges for a small amount depending on enemies hit.
* Fix : Fixed the I-Charge+Discharge direction being displayed incorrectly when charging for a single frame.
* Fix : Charge+Discharge having poor responsiveness while surfing.
* Fix : Laser Turbine implementations.

## Surfer 0.1.8
* Fix : Changed Fusion to be locked if IRL is not available.
* Fix : Fusion no longer gives use items that are in the fuse tiers.
* Fix : Fusion now drops Hoof and Syringe if the Fix mod is loaded.
* Fix : Fusion now only gives items that have IRL removal functions.
* Fix : Fixed description.txt and included the dependencies.
* Fix : Fixed the metadata and added the dependencies.

## Surfer 0.1.7
* Change : Buffed II-Charge+Discharge's initial damage
* Change : Buffed I-Charge+Discharge's chain range
* Change : Increased the damage of 360
* Change : Visual Only : Added III-Charge+Discharge sprite
* Change : Buffed III-Charge+Discharge heal
* Change : Decreased the vertical range of 360
* Change : Visual Only : Changed the alpha of the shock effect
* Change : Visual Only : Added charging sprites
* Change : Visual Only : Changed the sprite of II-Charge+Discharge
* Change : Added 5 artifacts and an item
* Change : Reintroduced minimum dimensions to I-Charge+Discharge
* Fix : Changed the skill descriptions
* Fix : Visual Only : The pond under the surfer is now behind enemies

## Surfer 0.1.6
* Change : Updated skill descriptions
* Change : Discharge has been renamed to Charge+Discharge
* Change : Added third Charge+Discharge skill
* Change : The range of 360 has been decreased, the charge gained per enemy has been buffed (Same as 0.1.4)
* Fix : I-Charge+Discharge is now more responsive
* Fix : Removed leftover print calls
* Fix : Discharge now obeys cooldown reduction
* Fix : Added proper Scepter effect
* Fix : Added Shattered Mirror handling	

## Surfer 0.1.5
* Change : Discharge has been reworked
* Change : You gain less charge per enemy with 360
* Change : Lowered the cooldown of Cosmic Waves from 6 to 4 seconds
* Change : Waves no longer push enemies
* Change : Increased 360's area of effect
* Change : Shocked enemies can't have the wet buff, waves will not grow on shocked enemies
* Fix : Visual Only : Idle sprite being displayed between stages
* Fix : Not being able to surf at desired height on left-moving waves
* Trivial : Increased the volume of the waves

## Surfer 0.1.4
* Change : Bigger enemies are pushed less
* Change : Non-Classic enemies aren't being pushed
* Change : Buffed the range of the initial discharge and added the amount to stats
* Change : Added the decoy sprite
* Change : Visual Only : The board follows the camera on death
* Fix : Checking for instance validity
* Fix : Variables not being cleared between stages/runs
* Fix : Visual Only : Discharge effect not being displayed on non-buffable enemies
* Trivial : Visual Only : Fixed time being reset on every surfer initialization

## Surfer 0.1.3
* Fix : Buffs not being applied to certain enemies
* Fix : Waves carrying to the next stage

## Surfer 0.1.1
* Fix : Enemies are now in front of the waves

## Surfer 0.1.0
* Initial surfer release