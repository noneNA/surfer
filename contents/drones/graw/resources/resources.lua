-- MODNAME - PATH

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}

resources.sprites.idle = restre_spriteLoad("idle.png", 6, 8, 8)

resources.sprites.item = restre_spriteLoad("item.png", 1, 8, 32)

return resources