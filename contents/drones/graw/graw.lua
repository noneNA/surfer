-- Surfer - contents/drones/graw/graw.lua

-- Dependencies:
---- CycloneLib.Rectangle
---- CycloneLib.Vector2
---- CycloneLib.modloader
---- GlobalItem

if not GlobalItem then return end

local resources = restre_require("resources/resources")

local o_heal = Object.find("EfHeal2", "Vanilla")

local HIT_CORRECT = 32
local DAMAGE = 0.4
local SPRITE_SPEED = 0.16

local LEECH_STEP = 3.2
local LEECH_WAVE = 4
local LEECH_BUBBLE_SIZE = 0.8
local LEECH_AMPLITUDE = 2
local LEECH_HEAL = 1/4

local ORBIT_SIZE = 2.4
local ORBIT_SPEED = 3.2
--local ORBIT_SQUASH = 1/4
local ORBIT_SQUASH = 1
local ORBIT_FLY = 0
local SPEED = 3.2

local COST = 64
local SPAWN_COST = 128
local RARITY = 4

local STAGES = {
	"Desolate Forest",
	"Sky Meadow",
	"Temple of the Elders",
	"Hive Cluster",
	"Damp Caverns",
}

-- Doesn't account for sprite changes (activity changes)
-- This should be somewhere else since all actors use it (Probably is but I don't know where)
local function metFrame(instance, frame)
	local last_subimage = instance:get("last_subimage") or instance.subimage
	return instance.subimage > frame and (instance.subimage < last_subimage or frame > last_subimage)
end

local function mirrorItems(i_from, i_to)
	if not GlobalItem.actorIsInit(i_to) then
		GlobalItem.initActor(i_to)
	end
	for _,namespace in ipairs(CycloneLib.modloader.getNamespaces()) do
		for _,item in ipairs(Item.findAll(namespace)) do
			--GlobalItem.remove(i_to, item)
			local count = i_from:countItem(item)
			if count > 0 then
				GlobalItem.addItem(i_to, item, count)
			end
		end
	end
end

local t = 0
callback.register("onStep", function() t = t + 1 end)
callback.register("onGameEnd", function() t = 0 end)

local function leechLine(fromx, fromy, tox, toy)
	graphics.color(Color.ROR_GREEN) ; graphics.alpha(1)
	--graphics.line(fromx, fromy, tox, toy)
	local s = CycloneLib.Vector2.new(fromx, fromy)
	local v = CycloneLib.Vector2.new(tox - fromx, toy - fromy)
	if v.length == 0 then return nil end
	local u = v:unit() * LEECH_STEP
	local length = (v / u.length).length
	for i=1,length do
		local p = u * i
		local d = p.x
		p = p + s
		graphics.circle(p.x, p.y + math.sin((t + d) / LEECH_WAVE) * LEECH_AMPLITUDE, LEECH_BUBBLE_SIZE)
		graphics.circle(p.x, p.y + math.cos((t + d) / LEECH_WAVE + math.pi/2) * LEECH_AMPLITUDE, LEECH_BUBBLE_SIZE)
	end
end

local o_sd = Object.new("SelfDestruct")
o_sd:addCallback("create", function(i_sd) i_sd:destroy() end)

local o_graw = Object.base("Drone", "Graw")

local o_grawitem = Object.base("DroneItem", "GrawItem")
o_grawitem.sprite = resources.sprites.item
o_grawitem:addCallback("create", function(i_grawitem)
	i_grawitem:set("name", "graw")
	i_grawitem:set("child", o_graw.id)
	i_grawitem:set("cost", math.ceil(COST * Difficulty.getScaling()))
end)
o_grawitem:addCallback("step", function(i_grawitem)
	if i_grawitem:collidesMap(i_grawitem.x, i_grawitem.y) then
		i_grawitem.y = i_grawitem.y - 1
	end
end)

local in_graw = Interactable.new(o_grawitem, "Graw")
in_graw.spawnCost = SPAWN_COST
for _,stage_name in ipairs(STAGES) do
	local stage = Stage.find(stage_name, "Vanilla")
	if not stage then error(stage_name) end
	stage.interactables:add(in_graw)
	stage.interactableRarity[in_graw] = RARITY
end

o_graw:addCallback("create", function(i_graw)
	i_graw
	--:set("child", -1)
	:set("child", o_sd.id)
	:set("x_range", 128)
	:set("y_range", 64)
	:set("last_subimage", 1)
	:set("persistent", 1)
	
	i_graw:setAnimation("idle", resources.sprites.idle)
	i_graw:setAnimation("idle_broken", resources.sprites.idle)
	i_graw.spriteSpeed = SPRITE_SPEED
end)

o_graw:addCallback("step", function(i_graw)
	local i_parent = Object.findInstance(i_graw:get("master") or -1)
	if not i_parent then
		return nil
	end
	if i_graw:get("master") ~= i_graw:get("old_master") then
		mirrorItems(i_parent, i_graw)
		i_graw:set("old_master", i_graw:get("master"))
	end
	
	if i_graw:get("state") == "chase" then
		local i_target = Object.findInstance(i_graw:get("target") or -1)
		if i_target then
			local r_master = CycloneLib.Rectangle.new(i_parent)
			
			local v_pos = CycloneLib.Vector2.new(1, 0) * ORBIT_SIZE * r_master.w
			v_pos.angle = t * ORBIT_SPEED
			v_pos.y = v_pos.y * ORBIT_SQUASH
			v_pos = CycloneLib.Vector2.new(r_master.x, r_master.top - ORBIT_FLY) + v_pos
			local d_pos = v_pos - CycloneLib.Vector2.new(i_graw)
			
			if d_pos.length > SPEED then
				d_pos = d_pos:unit() * SPEED
				--i_graw.xscale = math.sign(d_pos.x)
				i_graw.xscale = math.sign(i_target.x - i_graw.x)
				i_graw.x = i_graw.x + d_pos.x
				i_graw.y = i_graw.y + d_pos.y
			end

			if metFrame(i_graw, 2) then
				local x, y = i_target.x - HIT_CORRECT * i_graw.xscale, i_target.y
				local damager = i_graw:fireBullet(x, y, i_graw:getFacingDirection(), HIT_CORRECT, DAMAGE, nil, nil)
				damager:set("specific_target", i_target.id)
				o_heal:create(i_graw.x, i_graw.y):set("value", LEECH_HEAL * i_graw:get("damage"))
			end
		end
	end
	
	i_graw:set("last_subimage", i_graw.subimage)
end)

o_graw:addCallback("draw", function(i_graw)
	if i_graw:get("state") == "chase" then
		local i_target = Object.findInstance(i_graw:get("target") or -1)
		if i_target then
			leechLine(i_target.x, i_target.y, i_graw.x, i_graw.y)
		end
	end
end)

callback.register("onItemPickup", function(item, player)
	local item = Item.fromObject(item:getObject())
	for _,i_graw in ipairs(o_graw:findAll()) do
		if i_graw:get("master") == player.id then
			if not GlobalItem.actorIsInit(i_to) then
				GlobalItem.initActor(i_to)
			end
			GlobalItem.addItem(i_graw, item, 1)
		end
	end
end)