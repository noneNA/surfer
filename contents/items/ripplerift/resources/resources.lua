-- Surfer - contents/items/ripplerift/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}
resources.sounds = {}

resources.sprites.item = restre_spriteLoad("item.png", 2, 16, 16)

resources.sounds.ripple = restre_soundLoad("ripple.ogg")

return resources