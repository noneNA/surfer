-- Surfer - contents/items/ripplerift/ripplerift.lua

-- Dependencies:
---- CycloneLib.Vector2

local resources = restre_require("resources/resources")

local NAME = "Ripple Rift"
local TIER = "use"
local COOLDOWN = 120

local LIFE = 16 * 60
local DAMAGE_REDUCTION = 0.16
local RADIUS = 128
local SPEED = 1.4

local ALPHA = 0.6
local RIFT_COLOR = Color.fromRGB(167, 220, 220)

local COLOR = ({ ["common"] = "w", ["uncommon"] = "g", ["rare"] = "r", ["use"] = "or" })[TIER]

local p_enemies = ParentObject.find("enemies")

local ripplerift = Item.new(NAME)
ripplerift.displayName = NAME
ripplerift.pickupText = "Makes a field that attacks ripple in"
ripplerift.sprite = resources.sprites.item
ripplerift.color = COLOR
ripplerift.isUseItem = true
ripplerift.useCooldown = COOLDOWN

ripplerift:setTier(TIER)
ripplerift:setLog{
	group = TIER,
	description = string.format("Ripples all attacks with &y&%s%%&!& of their damage on AoE", (1 - DAMAGE_REDUCTION) * 100),
	priority = ({ ["common"] = "Standard", ["uncommon"] = "&g&Priority&!&", ["rare"] = "&r&High Priority&!&", ["use"] = "&y&Volatile&!&" })[TIER],
	destination = "U.F.R.L.\nOrbit\nLambda Scorpii b",
	date = "05/02/2056",
	story = "A inconveniently shaped device that can open fields to other dimensions at any time... or rather that was the original intent. It only works with one dimension and needs time to charge. It would surely be useful after a bit more development. For now though we don't have the necessary knowledge on it.",
}

local s_reverse
do
	local surface = Surface.new(2 * RADIUS, 2 * RADIUS)
	graphics.setTarget(surface)
	graphics.color(Color.BLACK) ; graphics.alpha(1)
	graphics.rectangle(0, 0, surface.width, surface.height)
	graphics.setBlendMode("subtract")
	graphics.circle(RADIUS, RADIUS, RADIUS)
	graphics.setBlendMode("normal")
	graphics.resetTarget()
	s_reverse = surface:createSprite(0, 0)
	s_reverse = s_reverse:finalize("Rift Reverse")
	surface:free()
end

local o_rift = Object.new("Rift")
local o_ripple = Object.new("Ripple")

o_rift:addCallback("create", function(i_rift)
	i_rift:set("life", LIFE)
	i_rift:getData().surface = Surface.new(2 * RADIUS, 2 * RADIUS)
end)
o_rift:addCallback("step", function(i_rift)
	i_rift:set("life", i_rift:get("life") - 1)
	if i_rift:get("life") <= 0 then
		i_rift:destroy()
		return nil
	end
end)
o_rift.depth = 12
o_rift:addCallback("draw", function(i_rift)
	local d_rift = i_rift:getData()
	local surface = d_rift.surface
	if (not surface:isValid()) then
		surface:free()
		surface = Surface.new(2 * RADIUS, 2 * RADIUS)
		d_rift.surface = surface
	end
	
	surface:clear()
	graphics.setTarget(surface)
	
	graphics.setBlendMode("additive")
	graphics.color(RIFT_COLOR) ; graphics.alpha(ALPHA)
	graphics.circle(RADIUS, RADIUS, RADIUS)
	graphics.circle(RADIUS, RADIUS, RADIUS - 1, true)
	graphics.setBlendMode("normal")
	
	for _,i_ripple in ipairs(o_ripple:findAll()) do
		graphics.color(Color.WHITE) ; graphics.alpha(ALPHA)
		graphics.circle(i_ripple.x - i_rift.x + RADIUS, i_ripple.y - i_rift.y + RADIUS, math.floor(i_ripple:get("radius")), true)
		--[[
		graphics.circle(i_ripple.x - i_rift.x + RADIUS, i_ripple.y - i_rift.y + RADIUS, math.floor(i_ripple:get("radius")), true)
		for i=-1,1 do
			graphics.circle(i_ripple.x - i_rift.x + RADIUS, i_ripple.y - i_rift.y + RADIUS, math.floor(i_ripple:get("radius")) + i, true)
		end
		--]]
	end
	
	graphics.setBlendMode("subtract")
	graphics.alpha(1)
	s_reverse:draw(0, 0)
	graphics.setBlendMode("normal")
	
	graphics.resetTarget()
	
	surface:draw(i_rift.x - RADIUS, i_rift.y - RADIUS)
end)

o_ripple:addCallback("create", function(i_ripple)
	i_ripple:set("life", LIFE)
	i_ripple:set("radius", 0)
	i_ripple:getData().hits = {}
end)
o_ripple:addCallback("step", function(i_ripple)
	i_ripple:set("life", i_ripple:get("life") - 1)
	if i_ripple:get("life") <= 0 then
		i_ripple:destroy()
		return nil
	end
	
	i_ripple:set("radius", i_ripple:get("radius") + 1)
	if i_ripple:get("radius") >= 2 * RADIUS then
		i_ripple:destroy()
		return nil
	end
	
	local radius = i_ripple:get("radius")
	local d_ripple = i_ripple:getData()
	local hits = d_ripple.hits
	local parent = Object.findInstance(i_ripple:get("parent") or -1)
	if not parent then return nil end
	local i_rift = o_rift:findNearest(i_ripple.x, i_ripple.y)
	if not i_rift then return nil end
	for _,i_enemy in ipairs(p_enemies:findAllEllipse(i_rift.x - RADIUS, i_rift.y - RADIUS, i_rift.x + RADIUS, i_rift.y + RADIUS)) do
		local v_distance = CycloneLib.Vector2.new(i_ripple) - CycloneLib.Vector2.new(i_enemy)
		if (not hits[i_enemy]) and math.abs(v_distance:getLength() - radius) <= SPEED then
			i_enemy:set("hp", i_enemy:get("hp") - i_ripple:get("damage"))
			misc.damage(i_ripple:get("damage"), i_enemy.x, i_enemy.y, false, Color.WHITE)
			hits[i_enemy] = true
		end
	end
end)
--[[
o_ripple:addCallback("draw", function(i_ripple)
	graphics.color(Color.WHITE) ; graphics.alpha(1)
	graphics.circle(i_ripple.x, i_ripple.y, i_ripple:get("radius"), true)
end)
--]]

callback.register("onFire", function(damager)
	local v_damager = CycloneLib.Vector2.new(damager)
	local i_rift = o_rift:findNearest(damager.x, damager.y)
	local parent = damager:getParent()
	if not isa(parent, "PlayerInstance") then return nil end
	if i_rift and ((v_damager - CycloneLib.Vector2.new(i_rift)):getLength() < RADIUS) then
		o_ripple:create(damager.x, damager.y):set("damage", damager:get("damage") * (1 - DAMAGE_REDUCTION)):set("parent", parent.id)
		resources.sounds.ripple:play(1, 1)
	end
end)

ripplerift:addCallback("use", function(player)
	o_rift:create(player.x, player.y)
end)