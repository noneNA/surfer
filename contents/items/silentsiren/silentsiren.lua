-- Surfer - contents/items/silentsiren/silentsiren.lua

-- Dependencies:
---- CycloneLib.Rectangle

local resources = restre_require("resources/resources")

local NAME = "Silent Siren"
local COLOR = "g"

local RADIUS = 64
local DURATION = 20
local WAVE = 1
local WAVE_SPEED = 1/16
local ALPHA = 0.1
local LIGHT = 4
local LIGHT_WAVE = 1/16
local LIGHT_WAVE_SPEED = 1/16
local FADE = 4
local LIGHT_BASE = 1/2
local X_ADJ = 10
local Y_ADJ = 4

local ATTACK_SPEED_BASE = 1/4
local ATTACK_SPEED_STACK = 1/12
local SPEED_BASE = 1/2
local SPEED_STACK = 1/8
local MIN_ATTACK_SPEED = 1/4
local MIN_SPEED = 1/8

local silentsiren = Item.new(NAME)
silentsiren.displayName = NAME
silentsiren.pickupText = "Slows enemies around you"
silentsiren.sprite = resources.sprites.item
silentsiren.color = COLOR

silentsiren:setTier("uncommon")
silentsiren:setLog{
	group = "uncommon",
	description = "Reduces the &y&attack and movement speed&!& of enemies nearby",
	priority = "&g&Priority&!&",
	destination = "Drowa\nNeptune",
	date = "08/06/2077",
	story = "The perfect tool to lure the prey in and disable it from escaping one caught. Make sure to not stare at it for too long.",
}

local p_enemies = ParentObject.find("enemies")

local t = 0
local function resetT() t = 0 end
callback.register("onStep", function() t = t + 1 end)
callback.register("onGameStart", resetT)

local bait = Buff.new("Bait")
bait.sprite = resources.sprites.bait
bait:addCallback("start", function(i_enemy)
	-- For some reason I have to check here even having set before applying
	-- I think it's due to how buffs work but I'm not sure
	local count = i_enemy:get("silentsiren") or 0
	i_enemy:set("silentsiren_attack_speed", i_enemy:get("attack_speed"))
	i_enemy:set("silentsiren_speed", i_enemy:get("pHmax"))
	i_enemy:set("attack_speed", math.max(i_enemy:get("attack_speed") - ATTACK_SPEED_BASE - ATTACK_SPEED_STACK * count, MIN_ATTACK_SPEED))
	i_enemy:set("pHmax", math.max(i_enemy:get("pHmax") - SPEED_BASE - SPEED_STACK * count, MIN_SPEED))
end)
bait:addCallback("end", function(i_enemy)
	local count = i_enemy:get("silentsiren") or 0
	i_enemy:set("attack_speed", i_enemy:get("silentsiren_attack_speed")):set("silentsiren_attack_speed", nil)
	i_enemy:set("pHmax", i_enemy:get("silentsiren_speed")):set("silentsiren_speed", nil)
	i_enemy:set("silentsiren", nil)
end)

callback.register("onPlayerStep", function(player)
	local count = player:countItem(silentsiren)
	if count <= 0 then return nil end
	for _,i_enemy in ipairs(p_enemies:findAllEllipse(player.x - RADIUS, player.y - RADIUS, player.x + RADIUS, player.y + RADIUS)) do
		if not i_enemy:hasBuff(bait) then
			local siren = i_enemy:get("silentsiren") or 0
			if count > siren then
				i_enemy:set("silentsiren", count)
			end
			i_enemy:applyBuff(bait, DURATION)
		end
	end
end)

callback.register("onPlayerDraw", function(player)
	local count = player:countItem(silentsiren)
	if count <= 0 then return nil end
	local r_player = CycloneLib.Rectangle.new(player)
	local _x = r_player.centerx + player.xscale * (r_player.w + X_ADJ)
	local _y = r_player.top + math.sin(t * WAVE_SPEED) * WAVE - Y_ADJ
	graphics.alpha(ALPHA) ; graphics.color(Color.WHITE)
	for i=1,FADE do
		graphics.circle(_x - 1, _y, (LIGHT * i) * (math.sin(t * LIGHT_WAVE_SPEED) * LIGHT_WAVE + LIGHT_BASE))
	end
	graphics.drawImage{
		image = resources.sprites.siren,
		x = _x,
		y = _y,
		xscale = player.xscale,
	}
end)