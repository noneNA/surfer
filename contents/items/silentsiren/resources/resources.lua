-- Surfer - contents/items/silentsiren/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}

resources.sprites.item = restre_spriteLoad("item.png", 1, 16, 16)

resources.sprites.siren = restre_spriteLoad("siren.png", 1, 13, 10)

resources.sprites.bait = restre_spriteLoad("bait.png", 1, 8, 8)

return resources