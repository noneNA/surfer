-- Surfer - contents/items/shades/shades.lua

-- Dependencies:
---- CycloneLib.Rectangle

local resources = restre_require("resources/resources")

local NAME = "Shades"
local COLOR = "w"

local BLOCK = 0.5
local CHANCE_BASE = 24
local CHANCE_STACK = 16
local DISTANCE = 4

local shades = Item.new(NAME)
shades.displayName = NAME
shades.pickupText = "Chance to block half of the damage when hit"
shades.sprite = resources.sprites.item
shades.color = COLOR

shades:setTier("common")
shades:setLog{
	group = "common",
	description = string.format("Gives &y&%s%%&!& chance to block &or&%s%%&!& of the damage when hit", CHANCE_BASE, BLOCK),
	priority = "Standard",
	destination = "Coast Hyde\nNeptune",
	date = "12/03/2024",
	story = "Wearing these shades makes you feel safer. The world seems much less important now. Only what's in front of you matters.",
}

callback.register("onHit", function(damager, i_hit)
	local damage = damager:get("shades_damage")
	if damage then
		damager:set("damage", damage):set("shades_damage", nil)
	end
	if isa(i_hit, "PlayerInstance") then
		local count = i_hit:countItem(shades)
		if (count > 0) and math.chance(math.min(CHANCE_BASE + CHANCE_STACK * (count - 1), 100)) then
			local damage = damager:get("damage")
			local block = damage * BLOCK
			local r_hit = CycloneLib.Rectangle.new(i_hit)
			damager:set("shades_damage", damage):set("damage", damage - block)
			misc.damage(block, r_hit.left - i_hit.xscale * DISTANCE, r_hit.top, false, Color.BLACK)
		end
	end
end)