-- Surfer - contents/items/shades/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}

resources.sprites.item = restre_spriteLoad("shades_item", "item.png", 1, 16, 16)

return resources