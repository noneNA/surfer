-- Surfer - contents/items/tidespromise/tidespromise.lua

-- Dependencies:
---- CycloneLib.Rectangle

local resources = restre_require("resources/resources")

local NAME = "Tide's Promise"
local COLOR = "w"

local BLOCK_BASE = 8
local BLOCK_STACK = 4

local DISTANCE = 4

local tidespromise = Item.new(NAME)
tidespromise.displayName = NAME
tidespromise.pickupText = "Block some of the damage when hit"
tidespromise.sprite = resources.sprites.item
tidespromise.color = COLOR

tidespromise:setTier("common")
tidespromise:setLog{
	group = "common",
	description = string.format("Blocks &y&%s&!& damage on each attack", BLOCK_BASE),
	priority = "Standard",
	destination = "U.F.R.L.\nOrbit\nLambda Scorpii b",
	date = "05/02/2056",
	story = "These shells are believed to have some sort of protective effect and are collected because of it. It is unknown whether the rumors are true since one wasn't obtained by researchers until this one. Let's hope that we can finally uncover the mystery.",
}

callback.register("onHit", function(damager, i_hit)
	local damage = damager:get("tidespromise_damage")
	if damage then
		damager:set("damage", damage):set("tidespromise_damage", nil)
	end
	if isa(i_hit, "PlayerInstance") then
		local count = i_hit:countItem(tidespromise)
		if count > 0 then
			local damage = damager:get("damage")
			local block = math.min(damage, BLOCK_BASE + BLOCK_STACK * (count - 1))
			local r_hit = CycloneLib.Rectangle.new(i_hit)
			damager:set("tidespromis_damage", damage):set("damage", damage - block)
			misc.damage(block, r_hit.left + i_hit.xscale * DISTANCE, r_hit.top, false, Color.WHITE)
		end
	end
end)