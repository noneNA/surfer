-- Surfer - contents/items/staticreload/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}

resources.sprites.item = restre_spriteLoad("item.png", 1, 16, 16)

for i=1,5 do
	local _i = tostring(i)
	resources.sprites["charge_".._i] = restre_spriteLoad("charge_".._i..".png", 1, 16, 16)
end

return resources