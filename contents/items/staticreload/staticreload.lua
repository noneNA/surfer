-- Surfer - contents/items/staticreload/staticreload.lua

-- Dependencies:
---- Nothing

local resources = restre_require("resources/resources")

local NAME = "Static Reload"
local COLOR = "r"

local DAMAGE_BASE = 4 * 10^(-2)
local DAMAGE_STACK = 1 * 10^(-2)

local CHARGE_SPRITES = {
	resources.sprites.item,
	resources.sprites.charge_1,
	resources.sprites.charge_2,
	resources.sprites.charge_3,
	resources.sprites.charge_4,
	resources.sprites.charge_5,
}

local CHARGES = #CHARGE_SPRITES
local PERIOD = CHARGES * 40

local staticreload = Item.new(NAME)
staticreload.displayName = NAME
staticreload.pickupText = "Overcharges your attacks periodically"
staticreload.sprite = resources.sprites.item
staticreload.color = COLOR

staticreload:setTier("rare")
staticreload:setLog{
	group = "rare",
	description = string.format("&b&Overcharges&!& attacks boosting damage by &y&%s%%&!& of enemy health", DAMAGE_BASE * 100),
	priority = "&r&High Priority&!&",
	destination = "Res-C\nKleptica\nNeptune",
	date = "18/11/2056",
	story = "This battery only provides energy when it desires to, which while quite inconvenient for regular use can still be put to alternate use cases. At least its not AC.",
}

local t = 0
local function resetT() t = 0 end
callback.register("onGameEnd", resetT)
callback.register("onStep", function() t = t + 1 end)

local sprite_index = 1
local function resetSpriteIndex() sprite_index = 1 end
callback.register("onGameEnd", resetSpriteIndex)

callback.register("onStep", function()
	if t % PERIOD == 0 then
		for _,player in ipairs(misc.players) do
			local count = player:countItem(staticreload)
			if count > 0 then
				player:set("staticreload", DAMAGE_BASE + DAMAGE_STACK * (count - 1))
			end
		end
	end
	
	local new_sprite_index = math.floor((t / (PERIOD / CHARGES)) % CHARGES) + 1
	if new_sprite_index ~= sprite_index then
		sprite_index = new_sprite_index
		for _,player in ipairs(misc.players) do
			player:setItemSprite(staticreload, CHARGE_SPRITES[sprite_index])
		end
	end
end)

callback.register("onFire", function(damager)
	local parent = damager:getParent()
	if isa(parent, "PlayerInstance") then
		damager:set("staticreload", parent:get("staticreload"))
		parent:set("staticreload", nil)
	end
end)

callback.register("onHit", function(damager, i_hit)
	local damage = damager:get("staticreload")
	if damage then
		damage = damage * i_hit:get("maxhp")
		i_hit:set("hp", i_hit:get("hp") - damage)
		misc.damage(damage, i_hit.x, i_hit.y, false, Color.GRAY)
	end
end)