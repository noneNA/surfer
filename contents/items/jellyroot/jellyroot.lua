-- Surfer - contents/items/jellyroot/jellyroot.lua

-- Dependencies:
---- Nothing

local resources = restre_require("resources/resources")

local jellyroot = Item.new("Jelly Root")
jellyroot.pickupText = "Gain speed when enemies are nearby"
jellyroot.sprite = resources.sprites.item
jellyroot:setTier("rare")
jellyroot:setLog {
	group = "rare",
	description = "Gain &y&speed&!& the closer you are to enemies",
	story = "It once soared the skies. Lost it's ability to move so that you wouldn't.",
	destination = "Nudzet Lake,\nHegal,\nNeptune",
	date = "2/11/54",
}

local SPEED_BOOST = 100
local THRESHOLD = 200

local boosted = {}
local basespeeds = {}
local speeds = {}
local p_enemies = ParentObject.find("enemies")
local particle = ParticleType.find("PixelDust","vanilla")
local oldColor = Color.fromRGB(255,255,255)
local newColor = Color.fromRGB(0,200,200)
local function resetJellyRoot() boosted, basespeeds, speeds = {}, {}, {} end
registercallback("onGameStart", resetJellyRoot)
jellyroot:addCallback("pickup", function(player)
	boosted[player] = (boosted[player] or 0) + 1
end)
registercallback("onStep", function()
	for k,v in pairs(boosted) do
		if (not k:isValid()) or (not (k:countItem(jellyroot) > 0)) then
			boosted[k] = nil
			speeds[k] = nil
			basespeeds[k] = nil
		else
			boosted[k] = k:countItem(jellyroot)
		end
	end
	for k,v in pairs(boosted) do if k:isValid() then
		local sdiff = k:get("pHmax") - (speeds[k] or 0)
		if sdiff ~= 0 then basespeeds[k] = (basespeeds[k] or 0) + sdiff end
		local _near = p_enemies:findNearest(k.x,k.y)
		if _near then
			local xdiff = (_near.x - k.x)
			local ydiff = (_near.y - k.y)
			local diff = math.sqrt(xdiff^2 + ydiff^2)
			if diff < THRESHOLD then
				local _var = math.clamp(diff, THRESHOLD/3, THRESHOLD)
				k:set("pHmax", basespeeds[k] +
					(1/_var) * SPEED_BOOST * math.clamp(math.sqrt(boosted[k])-0.5,0.5,5)
				)
				particle:color(newColor)
				particle:burst("below",k.x,k.y + (math.random() - 1/2) * 10, 1)
				particle:color(oldColor)
			else
				k:set("pHmax", basespeeds[k])
			end
		else
			k:set("pHmax", basespeeds[k])
		end
		speeds[k] = k:get("pHmax")
	end end
end)

if itemremover then
	itemremover.setRemoval(jellyroot,function(player, currentcount)
		if currentcount == 0 then boosted[player] = nil
		else boosted[player] = currentcount end
	end)
end