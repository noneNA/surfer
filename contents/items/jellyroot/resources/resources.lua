-- Surfer - contents/items/jellyroot/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}

resources.sprites.item = restre_spriteLoad("jellyroot_item.png", 1, 15, 15)

return resources