-- Surfer - contents/items/nightsguidance/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}
resources.particles = {}

resources.sprites.item = restre_spriteLoad("nightsguidance_item", "item.png", 2, 14, 14)

local star = ParticleType.new("Star")
star:shape("star")
star:color(Color.WHITE)
star:alpha(1)
star:scale(0.1, 0.1)
star:angle(0, 0, 32, 0, true)
star:life(2, 2)
resources.particles.star = star

local spark = ParticleType.new("Spark")
spark:shape("pixel")
spark:color(Color.WHITE)
spark:alpha(0.4, 0)
spark:life(16, 32)
resources.particles.spark = spark

return resources