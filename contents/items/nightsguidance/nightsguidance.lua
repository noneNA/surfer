-- Surfer - contents/items/nightsguidance/nightsguidance.lua

-- Dependencies:
---- CycloneLib.Vector2

local resources = restre_require("resources/resources")

local NAME = "Night's Guidance"
local COLOR = "or"
local COOLDOWN = 8

local SPEED = 1.2
local ARRIVE = 8
local DISTANCE = 4

local nightsguidance = Item.new(NAME)
nightsguidance.displayName = NAME
nightsguidance.pickupText = "Takes you to the exit"
nightsguidance.sprite = resources.sprites.item
nightsguidance.color = COLOR
nightsguidance.isUseItem = true
nightsguidance.useCooldown = COOLDOWN

nightsguidance:setTier("use")
nightsguidance:setLog{
	group = "use",
	description = "Shows you the way to the teleporter",
	priority = "Volatile",
	destination = "Grand Seaport\nNitca Ocean\nNeptune",
	date = "16/11/2056",
	story = "The second-best friend of anyone who lost their way in the vast cosmos. Should lead you right where you need to be.",
}

local o_teleporter = Object.find("Teleporter")
local o_star = Object.new("Star")
o_star:addCallback("create", function(i_star)
	local i_teleporter = o_teleporter:findNearest(i_star.x, i_star.y)
	if i_teleporter then
		i_star:set("teleporter_id", i_teleporter.id)
	end
end)
o_star:addCallback("step", function(i_star)
	local i_teleporter = Object.findInstance(i_star:get("teleporter_id"))
	if (not i_teleporter) or (not i_teleporter:isValid()) then
		i_star:destroy()
		return nil
	end
	
	local v_star = (CycloneLib.Vector2.new(i_teleporter) - CycloneLib.Vector2.new(i_star))
	if v_star.length < ARRIVE then
		i_star:destroy()
		return nil
	end
	v_star = SPEED * v_star:unit()
	i_star.x = i_star.x + v_star.x
	i_star.y = i_star.y + v_star.y
	
	resources.particles.spark:burst(
		"above",
		i_star.x + (math.random() - 1/2) * DISTANCE,
		i_star.y + (math.random() - 1/2) * DISTANCE,
		1
	)
	resources.particles.star:burst("above", i_star.x, i_star.y, 1)
end)

nightsguidance:addCallback("use", function(player)
	o_star:create(player.x, player.y)
end)