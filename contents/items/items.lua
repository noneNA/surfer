-- Surfer - contents/items/items.lua

-- Dependencies:
---- Nothing

local items = {
"jellyroot",
"miniaturestar",
"exochip",
"shades",
"tidespromise",
"leechingseedling",
"nightsguidance",
"silentsiren",
"staticreload",
"ripplerift",
}

for _,name in ipairs(items) do
	restre_require(string.format("%s/%s",name,name))
end