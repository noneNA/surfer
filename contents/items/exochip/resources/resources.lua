-- Surfer - contents/items/exochip/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}

resources.sprites.item = restre_spriteLoad("exochip_item", "item.png", 1, 16, 16)

return resources