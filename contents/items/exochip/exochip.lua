-- Surfer - contents/items/exochip/exochip.lua

-- Dependencies:
---- Nothing

local resources = restre_require("resources/resources")

local NAME = "Exochip"
local COLOR = "w"

local ARMOR = 0.2

local exochip = Item.new(NAME)
exochip.displayName = NAME
exochip.pickupText = string.format("Increases armor by %s%%", ARMOR * 100)
exochip.sprite = resources.sprites.item
exochip.color = COLOR

exochip:setTier("common")
exochip:setLog{
	group = "common",
	description = string.format("Increases armor by &b&%s%%&!&", ARMOR * 100),
	priority = "Standard",
	destination = "U.F.R.L.\nOrbit\nLambda Scorpii b",
	date = "05/02/2056",
	story = "Remains of an old experiment made on certain aquatic animals in order to increase their mental capabilities.",
}

exochip:addCallback("pickup", function(player)
	player:set("armor", player:get("armor") * (1 + ARMOR))
end)