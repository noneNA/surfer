-- Surfer - contents/items/miniaturestar/miniaturestar.lua

-- Dependencies:
---- Nothing

local resources = restre_require("resources/resources")

local NAME = "Miniature Star"
local COLOR = "g"

local DAMAGE_BASE = 4*10^(-3)
local DAMAGE_STACK = 2*10^(-3)
local RADIUS = 64

local p_enemies = ParentObject.find("enemies")

local miniaturestar = Item.new(NAME)
miniaturestar.displayName = NAME
miniaturestar.pickupText = "Nearby enemies take more damage on the next attack."
miniaturestar.sprite = resources.sprites.item
miniaturestar.color = COLOR

miniaturestar:setTier("uncommon")
miniaturestar:setLog{
	group = "uncommon",
	description = string.format(
		"&y&Burns&!& nearby enemies, boosting the next attack by &or&%s%%/s&!&",
		DAMAGE_BASE * 100 * 60
	),
	priority = "&g&Priority/Unstable&!&",
	destination = "U.F.R.L.\nOrbit\nLambda Scorpii b",
	date = "05/02/2056",
	story = "WARNING: The material can reach extremely high temperatures. Turning off the safety mechanisms is highly discouraged.",
}

callback.register("onStep", function()
	for _,player in ipairs(misc.players) do
		local x,y = player.x, player.y
		local count = player:countItem(miniaturestar)
		if count > 0 then
			for _,i_enemy in ipairs(p_enemies:findAllEllipse(x - RADIUS, y - RADIUS, x + RADIUS, y + RADIUS)) do
				i_enemy:set("pocketsun_damage", (i_enemy:get("pocketsun_damage") or 0) + DAMAGE_BASE + DAMAGE_STACK * (count - 1))
			end
		end
	end
end)

callback.register("onHit", function(damager, hit)
	local pocketsun_damage = hit:get("pocketsun_damage") or 0
	if pocketsun_damage > 0 then
		local damage = math.max(pocketsun_damage * damager:get("damage"), 1)
		hit:set("hp", hit:get("hp") - damage)
		hit:set("pocketsun_damage", 0)
		misc.damage(damage, hit.x, hit.y, false, resources.colors.burn)
	end
end)

callback.register("onDraw", function()
	for _,player in ipairs(misc.players) do
		local count = player:countItem(miniaturestar)
		if count > 0 then
			local x,y = player.x, player.y
			for _,i_enemy in ipairs(p_enemies:findAllEllipse(x - RADIUS, y - RADIUS, x + RADIUS, y + RADIUS)) do
				local r_enemy = CycloneLib.Rectangle.new(i_enemy)
				resources.particles.burn:burst(
					"above",
					r_enemy.x + math.random() * r_enemy.w,
					r_enemy.y + math.random() * r_enemy.h,
					1
				)
			end
		end
	end
end)