-- Surfer - contents/items/pocketsun/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}
resources.particles = {}
resources.colors = {}

resources.sprites.item = restre_spriteLoad("miniaturestar_item", "item.png", 1, 16, 16)

resources.colors.burn = Color.fromRGB(242,232,22)

local burn = ParticleType.new("Burn")
burn:shape("pixel")
burn:color(resources.colors.burn)
burn:alpha(0.64, 0)
burn:speed(0.6, 0.8, 0, 0)
burn:direction(90, 90, 0, 0)
burn:life(20, 40)
burn:scale(0.64, 0.64)
resources.particles.burn = burn

return resources