-- Surfer - contents/items/leechingseedling/leechingseedling.lua

-- Dependencies:
---- Nothing

local resources = restre_require("resources/resources")

local NAME = "Leeching Seedling"
local COLOR = "r"

local HEAL_BASE = (1/4)*10^(-1)
local HEAL_STACK = (1/8)*10^(-1)

local leechingseedling = Item.new(NAME)
leechingseedling.displayName = NAME
leechingseedling.pickupText = "Dealing damage &g&heals&!& you based on &r&damage dealt&!&"
leechingseedling.sprite = resources.sprites.item
leechingseedling.color = COLOR

leechingseedling:setTier("rare")
leechingseedling:setLog{
	group = "rare",
	description = string.format("Heal for &y&%s%%&!& of the damage you deal", HEAL_BASE * 100),
	priority = "High Priority/Biological",
	destination = "3030 Matlock,\nProd. Facility\nMars",
	date = "08/4/2056",
	story = "One of the seeds seem to have stopped growing at the perfect state for further use due to mutations. We're also sending this one there immediately. It might prove to be useful.",
}

callback.register("onHit", function(damager, i_hit)
	local parent = damager:getParent()
	if isa(parent, "PlayerInstance") then
		local count = parent:countItem(leechingseedling)
		if count > 0 then
			local heal = math.max(1, damager:get("damage") * (HEAL_BASE + HEAL_STACK * (count - 1)))
			parent:set("leech_heal", (parent:get("leech_heal") or 0) + heal)
		end
	end
end)

callback.register("onPlayerStep", function(player)
	local heal = player:get("leech_heal")
	if heal then
		player:set("hp", player:get("hp") + heal):set("leech_heal", nil)
		misc.damage(heal, player.x, player.y, false, Color.ROR_GREEN)
	end
end)