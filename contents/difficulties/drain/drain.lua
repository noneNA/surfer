-- Surfer - contents/difficulties/drain/drain.lua

-- Dependencies:
---- CycloneLib.table
---- CycloneLib.Rectangle
---- CycloneLib.Vector2

local resources = restre_require("resources/resources")

local MAP_OBJECTS = {
	"Chest1",
	"Chest2",
	"Chest3",
	"Chest4",
	"Chest5",
	"Shrine1",
	"Shrine2",
	"Shrine3",
	"Shrine4",
	"Shrine5",
}
for i,object_name in ipairs(MAP_OBJECTS) do
	MAP_OBJECTS[i] = Object.find(object_name)
end

local NAMESPACE_WHITELIST = {
	"Vanilla",
	"surfer",
}
local NAMESPACE_LOOKUP = CycloneLib.table.swap(NAMESPACE_WHITELIST)

local BANNED_ITEMS = {
	-- # Vanilla - Common
	"Warbanner",
	"Sticky Bomb",
	"Sprouting Egg",
	"Spikestrip",
	"Snake Eyes",
	"Rusty Blade",
	"Mortar Tube",
	"Meat Nugget",
	"Life Savings",
	"Lens Maker's Glasses",
	"Hermit's Scarf",
	"Headstompers",
	"Gasoline",
	"First Aid Kit",
	"Fire Shield",
	"Bundle of Fireworks",
	"Bustling Fungus",
	"Crowbar",
	"Barbed Wire",
	-- # Vanilla - Uncommon
	"56 Leaf Clover",
	"Arms Race",
	"Boxing Gloves",
	"Concussion Grenade",
	"Dead Man's Foot",
	"Energy Cell",
	"Filial Imprinting",
	"Frost Relic",
	"Golden Gun",
	"Harvester's Scythe",
	"Infusion",
	"Panic Mines",
	"Predatory Instincts",
	"Prison Shackles",
	"Smart Shopper",
	"Time Keeper's Secret",
	"Toxic Centipede",
	-- # Vanilla - Rare
	"Alien Head",
	"Beating Embryo",
	"Dio's Friend",
	"Fireman's Boots",
	"Happiest Mask",
	"Heaven Cracker",
	"Interstellar Desk Plant",
	"Laser Turbine",
	"Old Box",
	"Permafrost",
	"Plasma Chain",
	"Rapid Mitosis",
	"Repulsion Armor",
	"Telescopic Sight",
	"Tesla Coil",
	"Thallium",
	"The Hit List",
	"The Ol' Lopper",
	"Wicked Ring",
	
	-- # Surfer - Common
	"Tide's Promise",
	-- # Surfer - Uncommon
	"Miniature Star",
	"Silent Siren",
	-- # Surfer - Rare
	"Jelly Root",
	"Static Reload",
}

callback.register("postLoad", function()
	for i,item_name in ipairs(BANNED_ITEMS) do
		local item
		for _,namespace in ipairs(NAMESPACE_WHITELIST) do
			local found_item = Item.find(item_name, namespace)
			if found_item then
				item = found_item
			end
		end
		if not item then error(item_name) end
		BANNED_ITEMS[i] = item
	end
	BANNED_ITEMS = CycloneLib.table.swap(BANNED_ITEMS)
end)

local TIERS = {
	"common",
	"uncommon",
	"rare",
}

local ITEM_SIZE = 36

local NAME = "Drain"
local SCALE = 0.18
local SCALE_ONLINE = 0.0024
local ITEM_COUNT = { [TIERS[1]] = 3, [TIERS[2]] = 2, [TIERS[3]] = 1 }

local INDICATOR_RADIUS = 24
local INDICATOR_TSIZE = 8
local FIND_TEXT = "Find the teleporter."
local DIRECTIONS = {
	"East",
	"Southeast",
	"South",
	"Southwest",
	"West",
	"Northwest",
	"North",
	"Northeast",
}

local o_teleporter = Object.find("Teleporter")

local drain = Difficulty.new(NAME)
drain.displayName = NAME
drain.icon = resources.sprites.icon
drain.scale = SCALE
drain.scaleOnline = SCALE_ONLINE
drain.description = "&b&-DRAIN-&!&\nMain source of items are telepoter events.\nItem pools are limited."
drain.enableBlightedEnemies = true

-- Some instances slip for some reason
--[[
local destroy_packet
do
	local destroy = {}
	destroy_packet = net.Packet.new("drain_destroy", function(sender, net_mapobject)
		table.insert(destroy, net_mapobject)
	end)
	callback.register("onStep", function()
		for i,net_mapobject in pairs(destroy) do
			local mapobject = net_mapobject:resolve()
			if mapobject then
				mapobject:destroy()
				table.remove(destroy, i)
			end
		end
	end)
end

callback.register("onStageEntry", function()
	if net.host then
		if Difficulty.getActive() ~= drain then return nil end
		for _,object in ipairs(MAP_OBJECTS) do
			for _,instance in ipairs(object:findAll()) do
				if net.online then
					destroy_packet:sendAsHost(net.ALL, nil, instance:getNetIdentity())
				end
				instance:destroy()
			end
		end
	end
end)
--]]

callback.register("onStep", function()
	if Difficulty.getActive() ~= drain then return nil end
	for _,object in ipairs(MAP_OBJECTS) do
		for _,instance in ipairs(object:findAll()) do
			instance:destroy()
		end
	end
end)

local item_packet = net.Packet.new("drain_item", function(sender, net_teleporter, tier_index, item)
	local i_teleporter = net_teleporter:resolve()
	if i_teleporter then
		local d_telepoter = i_teleporter:getData()
		if not d_telepoter.chosen_items then
			d_telepoter.chosen_items = {}
		end
		if not d_telepoter.chosen_items[tier_index] then
			d_telepoter.chosen_items[tier_index] = {}
		end
		table.insert(d_telepoter.chosen_items[tier_index], item)
		net.localPlayer:giveItem(item, 1)
		i_teleporter:set("drained", 1)
	end
end)

callback.register("onStep", function()
	if net.host then
		if Difficulty.getActive() ~= drain then return nil end
		for _,i_teleporter in ipairs(o_teleporter:findAll()) do
			if (i_teleporter:get("active") >= 3) and (i_teleporter:get("drained") ~= 1) then
				local chosen_items = {}
				
				for tier_index,tier in pairs(TIERS) do
					local count = ITEM_COUNT[tier]
					
					local tier_items = {}
					for _,item in ipairs(ItemPool.find(tier):toList()) do
						if NAMESPACE_LOOKUP[item:getOrigin()] and (not BANNED_ITEMS[item]) then
							table.insert(tier_items, item)
						end
					end
					
					chosen_items[tier_index] = {}
					for i=1,count do
						table.insert(chosen_items[tier_index], table.irandom(tier_items))
					end
				end
				
				local net_teleporter = i_teleporter:getNetIdentity()
				--for _,player in ipairs(misc.players) do
					for tier_index,items in pairs(chosen_items) do
						for _,item in ipairs(items) do
							--player:giveItem(item, 1)
							if net.online then
								net.localPlayer:giveItem(item, 1)
								item_packet:sendAsHost(net.ALL, nil, net_teleporter, tier_index, item)
							else
								for _,player in ipairs(misc.players) do
									player:giveItem(item, 1)
								end
							end
						end
					end
				--end
				
				local d_telepoter = i_teleporter:getData()
				d_telepoter.chosen_items = chosen_items
				
				i_teleporter:set("drained", 1)
			end
		end
	end
end)

callback.register("onDraw", function()
	if Difficulty.getActive() ~= drain then return nil end
	for _,i_teleporter in ipairs(o_teleporter:findAll()) do
		if i_teleporter:get("drained") == 1 then
			local r_telepoter = CycloneLib.Rectangle.new(i_teleporter)
			local y = 1/2
			for tier_index,items in pairs(i_teleporter:getData().chosen_items) do
				local count = #items
				for i,item in ipairs(items) do
					local dx = ((i - 1) - (count - 1) / 2) * ITEM_SIZE
					graphics.drawImage{
						item.sprite,
						x = math.floor(r_telepoter.centerx + dx),
						y = math.floor(r_telepoter.top - ITEM_SIZE * y),
					}
				end
				y = y + 1
			end
		end
	end
end)

callback.register("onPlayerStep", function()
	if Difficulty.getActive() ~= drain then return nil end
	local hud = misc.hud
	local objective = hud:get("objective_text")
	if objective:find(FIND_TEXT) ~= nil then
		local i_teleporter = o_teleporter:find(1)
		local player = misc.players[1]
		
		local v_player = CycloneLib.Vector2.new(player)
		local v_teleporter = CycloneLib.Vector2.new(i_teleporter)
		local v_diff = v_teleporter - v_player
		
		local angle = v_diff.angle
		local angle_index = math.round((angle/360) * #DIRECTIONS)
		angle_index = (angle_index % (#DIRECTIONS)) + 1
		local direction = DIRECTIONS[angle_index]
		
		hud:set("objective_text", string.format("%s (%s)", FIND_TEXT, direction))
	end
end)

--[=[
callback.register("onPlayerDraw", function(player)
	if Difficulty.getActive() ~= drain then return nil end
	local r_player = CycloneLib.Rectangle.new(player)
	local i_teleporter = o_teleporter:find(1)
	if not i_teleporter then return nil end
	
	local v_player = CycloneLib.Vector2.new(player)
	local v_teleporter = CycloneLib.Vector2.new(i_teleporter)
	local v_diff = v_teleporter - v_player
	
	local v_base = v_player + (v_diff:unit() * (INDICATOR_RADIUS + INDICATOR_TSIZE / 2))
	local v_perp = (CycloneLib.Vector2.new(1, -(v_diff.x/v_diff.y)):unit()) * INDICATOR_TSIZE
	
	local v1 = v_base + (v_diff:unit() * INDICATOR_TSIZE)
	local v2 = v_base + v_perp
	local v3 = v_base - v_perp
	
	graphics.color(Color.ROR_YELLOW) ; graphics.alpha(0.4)
	graphics.circle(r_player.centerx, r_player.centery, INDICATOR_RADIUS, true)
	graphics.triangle(
		math.floor(v1.x), math.floor(v1.y),
		math.floor(v2.x), math.floor(v2.y),
		math.floor(v3.x), math.floor(v3.y)
	)
	
	--[[
	graphics.color(Color.BLACK)
	graphics.circle(v1.x, v1.y, 4)
	graphics.circle(v2.x, v2.y, 4)
	graphics.circle(v3.x, v3.y, 4)
	graphics.circle(v_base.x, v_base.y, 4)
	--]]
end)
--]=]