-- Surfer - contents/artifacts/artifacts.lua

-- Dependencies:
---- Nothing

local artifacts = {
"magnet",
"fusion",
"rain",
--"collector",
--"reincarnation",
"siphon",
}

for _,name in ipairs(artifacts) do
	restre_require(string.format("%s/%s",name,name))
end