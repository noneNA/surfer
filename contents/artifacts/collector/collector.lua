local collector = Artifact.new("collector")
collector.displayName = "Collector"
collector.unlocked = true
collector.loadoutSprite = sprites["collector"]
collector.loadoutText = "Combat stores health, lethal damage replenishes it."

local collection = {}
registercallback("onHit", function(damager)
	if collector.active then
		if damager and damager:isValid() and damager:getParent() and damager:getParent():isValid() then
			if (damager:getParent():getObject():getName() ~= "P") then
				collection[damager:getParent()] = (collection[damager:getParent()] or 0) + damager:get("damage") * 2
			else
				collection[damager:getParent()] = (collection[damager:getParent()] or 0) + 0.2
			end
		end
	end
end)
registercallback("preStep", function()
	if collector.active then
		for k,v in pairs(collection) do
			if not k:isValid() then collection[k] = nil end
		end
	end
end)
registercallback("onStep", function()
	if collector.active then
		for k,v in pairs(actors:findAll()) do
			if v:isValid() and (v:get("hp") < 0) then
				v:set("hp", v:get("hp") + (collection[v] or 0))
				if collection[v] then
					local _r = Cyclone.Rectangle:new()
					_r:fromInstance(v)
					transferp:burst("middle",_r.centerx,_r.top - 16,3)
					misc.shakeScreen(10)
					misc.damage(collection[v],_r.centerx,_r.top - 20,false,Color.GREEN)
				end
				collection[v] = nil
			end
		end
	end
end)