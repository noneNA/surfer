-- Surfer - contents/artifacts/rain/rain.lua

-- Dependencies:
---- CycloneLib.table
---- CycloneLib.misc

local resources = restre_require("resources/resources")

local rain = Artifact.new("Rain")
rain.displayName = "Rain"
rain.unlocked = true
rain.loadoutSprite = resources.sprites.loadout
rain.loadoutText = "Now it certainly rains."

--[[
local INTENSITY_SCALE = 1 * 100
local INTENSITY_MAX = 200 * 100
local DROP_CHANCE = 4

local drops = {}
local w,h = 0,0
local rx,ry = 0,0
local intensity = 0
local wind = 0
local pressure = 0
local wind_buffer = 0

local function rainReset()
	drops = {}
	w, h, rx, ry, wind, pressure, wind_buffer, rx, ry, w, h = 0,0,0,0,0,0,0,0,0,0,0
end
registercallback("onGameStart", rainReset)

local function newDrop()
	local _intensity = math.sqrt(math.sqrt(math.clamp(intensity,1,50)))
	local drop = {}
	drop.x = math.random(-w,w)
	drop.y = math.random(2,20)
	drop.s = math.random(4*_intensity*100,8*_intensity*100)/100
	drop.l = math.random(1,10)
	return drop
end

registercallback("onHUDDraw", function()
	if rain.active then
		w,h = graphics.getHUDResolution()
		rx, ry = CycloneLib.misc.screenToWorld(0,0)
		local _m,_s = misc.getTime()
		intensity = math.clamp((_m+(_s/60))*INTENSITY_SCALE,0,INTENSITY_MAX)
		intensity = INTENSITY_MAX

		for i=0,(42*intensity) do if math.chance(DROP_CHANCE) then
			table.insert(drops, newDrop())
		end end
		
		local _wind = math.random(-4*intensity*intensity,4*intensity*intensity)/1000
		if (math.abs(_wind) + 0.1) >= pressure then
			wind_buffer = _wind
			pressure = math.abs(_wind) * 1.2
		else
			pressure = pressure - 0.02*intensity*intensity
			local _sign = math.sign(wind_buffer)
			wind = wind + _sign/64
			wind_buffer = wind_buffer - _sign/64
		end
		
		graphics.alpha(0.40) ; graphics.color(Color.fromRGB(0,150,200))
		for k,v in ipairs(drops) do
			if v.y > h then CycloneLib.table.iremove(drops,k) else
				local _wind = wind * (math.random(100,200)/100)
				local _angle = math.atan((v.s / _wind))
				local _x = (v.x - rx)%w ; local _y = (v.y - ry)%h
				local _w = v.l * math.cos(_angle) ; local _h = v.l * math.sin(_angle)
				graphics.line(_x,_y,_x+_w,_y+_h)
				v.y = v.y + v.s
				v.x = v.x + _wind
			end
		end
		graphics.alpha(1)
	end
end)
--]]


---[[
local iremove = CycloneLib.table.iremove

local BASE_DROP_CHANCE = 0.8
local MAX_DROP_CHANCE = 32

local BASE_INTENSITY = 1 / (1 * 60)
local MAX_INTENSITY = 1
local INTENSITY_SCALE = 1 / (30 * 60) -- 30 Minutes

local BASE_SPEED = 4.2
local MAX_SPEED = 24

local BASE_WIND_SPEED = 2
local MAX_WIND_SPEED = 20
local PRESSURE_MULTIPLIER = 1 - 0.0002
local WIND_DIFF = 0.2

local DROP_ALPHA = 0.4
local DROP_COLOR = Color.fromRGB(0, 255/3, 255/2)

local wind_speed
local wind_buffer
local wind_pressure
local function resetWind()
	wind_speed = 0
	wind_buffer = 0
	wind_pressure = 0
end
resetWind()

local drops
local function resetDrops()
	drops = {
		x = {},
		y = {},
		v = {},
		--l = {},
	}
end
resetDrops()

--local function newDrop(x, y, v, l)
local function newDrop(x, y, v)
	table.insert(drops.x, x)
	table.insert(drops.y, y)
	table.insert(drops.v, v)
	--table.insert(drops.l, l)
end

local function removeDrop(i)
	iremove(drops.x, i)
	iremove(drops.y, i)
	iremove(drops.v, i)
end

local function dropAmount(chance)
	local full = math.floor(chance)
	local remain = chance - full
	return full + (math.chance(remain) and 1 or 0)
end

local function resetRain()
	resetWind()
	resetDrops()
end
callback.register("onGameEnd", resetRain)

callback.register("onHUDDraw", function()
	if rain.active then
		local w, h = graphics.getHUDResolution()
		--local sx, sy = CycloneLib.misc.screenToWorld(0, 0)
		local sx, sy = 0, 0
		local minutes, seconds = misc.getTime()
		local intensity = math.clamp((minutes * 60 + seconds) * INTENSITY_SCALE, BASE_INTENSITY, MAX_INTENSITY)
		
		local drop_chance = BASE_DROP_CHANCE + (MAX_DROP_CHANCE - BASE_DROP_CHANCE) * intensity
		--if math.chance(drop_chance) then
		for i=1,dropAmount(drop_chance) do
			local x = w/2 + ((math.random() - 1/2) * (2 * w))
			local y = 0
			--local v = math.random(4 * intensity * 100, 8 * intensity * 100)/100
			local v = BASE_SPEED + (MAX_SPEED - BASE_SPEED) * (intensity + math.random()) * 0.5
			--local l = BASE_LENGTH + (MAX_LENGTH - BASE_LENGTH) * math.random()
			newDrop(x, y, v)
		end
		
		local wind = (math.random() - 1/2) * (BASE_WIND_SPEED + (MAX_WIND_SPEED - BASE_WIND_SPEED) * (3/4 * intensity * intensity + 1/4 * math.random()))
		if math.abs(wind) >= wind_pressure then
			wind_buffer = wind - wind_speed
			wind_pressure = math.abs(wind)
		else
			wind_pressure = wind_pressure * PRESSURE_MULTIPLIER
			
			local wind_diff = math.sign(wind_buffer) * math.min(WIND_DIFF, math.abs(wind_buffer))
			wind_buffer = wind_buffer - wind_diff
			wind_speed = wind_speed + wind_diff
		end
		
		--graphics.color(Color.BLACK) ; graphics.alpha(1)
		--graphics.rectangle(0, 0, w, h)
		
		graphics.alpha(DROP_ALPHA) ; graphics.color(DROP_COLOR) ; graphics.setBlendMode("additive")
		local count, removed, i = #drops.x, 0, 1
		while i <= count - removed do
			local x, y, v = drops.x[i], drops.y[i], drops.v[i]
			
			-- Doesn't increment when removing to not skip (iremove)
			if y + v > h then
				removeDrop(i)
				removed = removed + 1
			else
				local _x, _y = (x - sx) % w, (y - sy) % h
				
				graphics.line(_x, _y, _x + wind_speed, _y + v)
				drops.x[i] = x + wind_speed
				drops.y[i] = y + v
				
				i = i + 1
			end
		end
		graphics.alpha(1) ; graphics.setBlendMode("normal")
	end
end)
--]]