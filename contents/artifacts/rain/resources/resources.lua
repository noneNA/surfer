-- Surfer - contents/artifacts/rain/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}

resources.sprites.loadout = restre_spriteLoad("rain_loadout.png", 2, 18, 18)

return resources