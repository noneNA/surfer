-- Surfer - contents/artifacts/reincarnation/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}
resources.particles = {}
resources.sounds = {}

resources.sprites.loadout = restre_spriteLoad("reincarnation_loadout.png", 2, 18, 18)

local transferp = ParticleType.new("transferp")
local transfern = ParticleType.new("transfern")
transferp:life(60,75)
transfern:life(60,75)
transferp:shape("sphere")
transfern:shape("sphere")
transferp:alpha(0.1,0)
transfern:alpha(0.1,0)
transferp:scale(0.1,0.1)
transfern:scale(0.1,0.1)
transferp:direction(30,150,0,30)
transfern:direction(30,150,0,30)
transferp:speed(0.2,0.4,0,0.1)
transfern:speed(0.2,0.4,0,0.1)
transferp:color(Color.fromRGB(0,255,0))
transfern:color(Color.fromRGB(255,0,0))
resources.particles.transferp = transferp
resources.particles.transfern = transfern

local hp_freeze = ParticleType.new("invincible")
hp_freeze:life(4,4)
hp_freeze:shape("ring")
hp_freeze:alpha(0.1,0.1)
hp_freeze:color(Color.fromRGB(255,255,0))
resources.particles.hp_freeze = hp_freeze

resources.sounds.reincarnate = Sound.find("Revive")

return resources