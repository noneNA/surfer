-- Surfer - contents/artifacts/reincarnation/reincarnation.lua

-- Dependencies:
---- CycloneLib

local HP_FREEZE_DURATION = 300
local DEFAULT_DEATHS = 3
local GATHER_MULTIPLIER = 0.1

local resources = restre_require("resources/resources")

local p_actors = ParentObject.find("actors")

local reincarnation = Artifact.new("Reincarnation")
reincarnation.displayName = "Reincarnation"
reincarnation.unlocked = true
reincarnation.loadoutSprite = resources.sprites.loadout
reincarnation.loadoutText = "Protects lethal damage by sacrifing others."

local hp_freeze = Buff.new("Frozen Health")
hp_freeze:addCallback("start", function(i_actor)
	i_actor:set("frozen_hp", i_actor:get("hp"))
end)
hp_freeze:addCallback("step", function(i_actor)
	local r = CycloneLib.Rectangle.new(i_actor)
	resources.particles.hp_freeze:burst("below", r.centerx, r.centery, 1)
	
	local frozen_hp = i_actor:get("frozen_hp")
	if i_actor:get("hp") < frozen_hp then
		i_actor:set("hp", frozen_hp)
	end
end)
hp_freeze:addCallback("end", function(i_actor)
	i_actor:set("frozen_hp", nil)
end)

local remaining_deaths = DEFAULT_DEATHS
local function resetRemainingDeaths() remaining_deaths = DEFAULT_DEATHS end
registercallback("onGameStart", resetRemainingDeaths)

local function reincarnate(i_actor)
	if remaining_deaths <= 0 then
		remaining_deaths = remaining_deaths + DEFAULT_DEATHS
		
		local r = CycloneLib.Rectangle:new(i_actor)
		resources.particles.transferp:burst("above", r.centerx, r.top - 16, 3)
		
		misc.shakeScreen(10)
		
		local gather_total = 0
		local gather_cap = i_actor:get("maxhp") - i_actor:get("hp")
		for k,i_other in pairs(p_actors:findAll()) do
			if i_other ~= i_actor then
				if gather_total < gather_cap then
					local r = CycloneLib.Rectangle:new(i_other)
					resources.particles.transfern:burst("middle", r.centerx, r.top - 16, 3)
					
					local gahter_single = i_other:get("maxhp") * GATHER_MULTIPLIER
					
					local damager = misc.fireBullet(i_other.x, i_other.y, 1, 1, gahter_single, "neutral")
					:set("specific_target", i_other.id)
					
					gather_total = gather_total + gahter_single
				end
			end
		end
		i_actor:set("hp", math.max(i_actor:get("hp"), 0) + gather_total)
		
		i_actor:applyBuff(hp_freeze, HP_FREEZE_DURATION)
		
		local sound = resources.sounds.reincarnate
		if not sound:isPlaying() then
			sound:play(1,1)
		end
	end
end
local function checkReincarnation()
	if reincarnation.active then
		for k,v in pairs(p_actors:findAll()) do
			if v:isValid() and (v:get("hp") <= 0) then reincarnate(v) end
		end
	end
end

registercallback("onNPCDeath", function() remaining_deaths = remaining_deaths - 1 ; checkReincarnation() end)
registercallback("onPlayerDeath", function() remaining_deaths = remaining_deaths - 1 ; checkReincarnation() end)

registercallback("onPlayerHUDDraw", function(player,x,y)
	if reincarnation.active then
		graphics.print(tostring(remaining_deaths), x+128,y - 16, graphics.FONT_CRITICAL)
	end
end)