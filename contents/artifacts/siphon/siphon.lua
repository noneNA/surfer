-- Surfer - contents/artifacts/siphon/siphon.lua

-- Dependencies:
---- CycloneLib.net
---- CycloneLib.Rectangle

local p_enemies = ParentObject.find("enemies")

local resources = restre_require("resources/resources")

local DROP_CHANCE = 32
local WAVE_SPEED = 1/16

local siphon = Artifact.new("Siphon")
siphon.displayName = "Siphon"
siphon.unlocked = GlobalItem ~= nil
siphon.loadoutSprite = resources.sprites.loadout
siphon.loadoutText = "Bosses have an additional item with a chance to drop them"
siphon.pickupName = "Artifact of Siphon"
--siphon.disabled = false

local SIPHON_ITEMS = {
"Barbed Wire",
"Bitter Root",
"Crowbar",
"Fire Shield",
"First Aid Kit",
"Hermit's Scarf",
"Lens Maker's Glasses",
"Mortar Tube",
"Mysterious Vial",
"Paul's Goat Hoof",
"Rusty Blade",
"Soldier's Syringe",
"Spikestrip",
"Sticky Bomb",
"Taser",
"AtG Missile Mk. 1",
"Boxing Gloves",
"Dead Man's Foot",
"Energy Cell",
"Filial Imprinting",
"Guardian's Heart",
"Harvester's Scythe",
"Leeching Seed",
"Panic Mines",
"Predatory Instincts",
"Prison Shackles",
"Tough Times",
"Toxic Centipede",
"Ukulele",
"AtG Missile Mk. 2",
"Brilliant Behemoth",
"Dio's Friend",
"Fireman's Boots",
"Heaven Cracker",
"Hyper-Threader",
"Laser Turbine",
"Permafrost",
"Plasma Chain",
"Repulsion Armor",
"Shattering Justice",
"Tesla Coil",
"Thallium",
}

local pool_siphon = ItemPool.new("Siphon")

for i,item_name in ipairs(SIPHON_ITEMS) do
	local item = Item.find(item_name)
	if not item then error(item_name) end
	pool_siphon:add(item)
end

local item_sync = CycloneLib.net.AllPacket("siphon_item", function(net_boss, item)
	local i_boss = net_boss:resolve()
	if i_boss then
		local d_boss = i_boss:getData()
		d_boss.siphon_item = item
		i_boss:set("siphon", 1)
		GlobalItem.addItem(i_boss, item)
	end
end)

local gi_init_sync = CycloneLib.net.AllPacket("globalitems_init", function(net_boss)
	local i_boss = net_boss:resolve()
	if i_boss and (not GlobalItem.actorIsInit(i_boss)) then
		GlobalItem.initActor(i_boss)
	end
end)

callback.register("onActorInit", function(i_actor)
	if net.host and siphon.active and i_actor:isBoss() then
		local item = pool_siphon:roll()
		local net_boss = i_actor:getNetIdentity()
		gi_init_sync(net_boss)
		item_sync(net_boss, item)
	end
end)

callback.register("onNPCDeath", function(i_actor)
	if net.host and i_actor:get("siphon") then
		if math.chance(DROP_CHANCE) then
			i_actor:getData().siphon_item:create(i_actor.x, i_actor.y)
		end
	end
end)

local t = 0
local function resetT() t = 0 end
callback.register("onGameEnd", resetT)
callback.register("onStep", function() t = t + 1 end)

callback.register("onDraw", function()
	for _,i_boss in ipairs(p_enemies:findMatching("siphon", 1)) do
		local item = i_boss:getData().siphon_item
		local r_boss = CycloneLib.Rectangle.new(i_boss)
		local cycle = ((t * WAVE_SPEED) / (4 * math.pi)) % 1
		cycle = not (7/8 > cycle and cycle > 3/8)
		graphics.drawImage{
			image = item.sprite,
			x = r_boss.centerx,
			y = r_boss.top - item.sprite.yorigin - 16,
			alpha = cycle and 0 or ((1 + math.sin(t * WAVE_SPEED)) / 2)
		}
	end
end)