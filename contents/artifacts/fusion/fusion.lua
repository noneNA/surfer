-- Surfer - contents/artifacts/fusion/fusion.lua

-- Dependencies:
---- IRL

local CHANCE_COMMON_TO_UNCOMMON = 64
local CHANCE_UNCOMMON_TO_RARE = 32

local resources = restre_require("resources/resources")

local remover = modloader.checkMod("item-removal-lib")
local fusion = Artifact.new("Fusion")
fusion.displayName = "Fusion"
fusion.loadoutSprite = resources.sprites.loadout
fusion.loadoutText = "Chance to fuse items when one is picked."
fusion.unlocked = remover
if remover then
	local common = ItemPool.find("common")
	local uncommon = ItemPool.find("uncommon")
	local rare = ItemPool.find("rare")
	local function itemToParticle(item)
		local _particle = ParticleType.find("ItemParticle"..item:getName())
		if not _particle then
			_particle = ParticleType.new("ItemParticle"..item:getName())
			_particle:life(60,60)
			_particle:alpha(1,0.8,0)
			_particle:sprite(item.sprite, false, false, false)
		end
		return _particle
	end
	local pool_commons = {}
	local pool_uncommons = {}
	local pool_rares = {}
	registercallback("onGameStart", function()
		for k,v in ipairs(rare:toList()) do if (not v.isUseItem) and (itemremover.getRemoval(v)) then pool_rares[v] = v end end
		for k,v in ipairs(uncommon:toList()) do if (not v.isUseItem) and (itemremover.getRemoval(v)) then pool_uncommons[v] = v end end
		for k,v in ipairs(common:toList()) do if (not v.isUseItem) and (itemremover.getRemoval(v)) then pool_commons[v] = v end end
	end)
	local function fuse(player)
		local commons = {}
		local uncommons = {}
		
		local count = 0
		for k,v in pairs(pool_commons) do if player:countItem(v) > 0 then
			commons[v] = v
			count = count + 1
		end end
		if count >= 3 and math.chance(CHANCE_COMMON_TO_UNCOMMON) then
			for i=1,3 do
				local _item = table.random(commons)
				commons[_item] = nil
				itemremover.removeItem(player, _item)
				itemToParticle(_item):burst("below",player.x - 32*2 + i*32,player.y - 48, 1)
			end
			local _item = table.random(pool_uncommons)
			player:giveItem(_item, 1)
			itemToParticle(_item):burst("below",player.x,player.y - 80, 1)
			if not resources.sounds.fuse:isPlaying() then resources.sounds.fuse:play(1,2) end
		end
		
		local count = 0
		for k,v in pairs(pool_uncommons) do if player:countItem(v) > 0 then
			uncommons[v] = v
			count = count + 1
		end end
		if count >= 3 and math.chance(CHANCE_UNCOMMON_TO_RARE) then
			for i=1,3 do
				local _item = table.random(uncommons)
				uncommons[_item] = nil
				itemremover.removeItem(player, _item)
				itemToParticle(_item):burst("below",player.x - 32*2 + i*32,player.y - 48, 1)
			end
			local _item = table.random(pool_rares)
			player:giveItem(_item, 1)
			itemToParticle(_item):burst("below",player.x,player.y - 80, 1)
			if not resources.sounds.fuse:isPlaying() then resources.sounds.fuse:play(1,2) end
		end
	end
	registercallback("onItemPickup", function(item,player)
		if fusion.active then
			fuse(player)
		end
	end)
end