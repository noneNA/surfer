-- Surfer - contents/artifacts/fusion/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}
resources.sounds = {}

resources.sprites.loadout = restre_spriteLoad("fusion_loadout.png", 2, 18, 18)

resources.sounds.fuse = Sound.find("Embryo")

return resources