-- Surfer - contents/artifacts/magnet/magnet.lua

-- Dependencies:
---- Nothing

local PULL_MULTIPLIER = 0.005

local resources = restre_require("resources/resources")

local magnet = Artifact.new("Magnet")
magnet.displayName = "Magnet"
magnet.unlocked = true
magnet.loadoutSprite = resources.sprites.loadout
magnet.loadoutText = "Pulls items towards you."

local p_items = ParentObject.find("items")
local o_player = Object.find("P")
registercallback("onStep", function()
	if magnet.active then
		for k,v in pairs(p_items:findAll()) do
			local _player = o_player:findNearest(v.x,v.y)
			local x,y = _player.x, _player.y
			v.x = v.x + (x - v.x) * PULL_MULTIPLIER
			v.y = v.y + (y - v.y) * PULL_MULTIPLIER
		end
	end
end)