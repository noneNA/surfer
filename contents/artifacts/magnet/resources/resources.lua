-- Surfer - contents/artifacts/magnet/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}
resources.particles = {}
resources.sounds = {}

resources.sprites.loadout = restre_spriteLoad("magnet_loadout.png", 2, 18, 18)

return resources