-- Surfer - contents/elites/wrecked/wrecked.lua

-- Dependencies:
---- CycloneLib.modloader
---- CycloneLib.Rectangle
---- CycloneLib.collision
---- CycloneLib.table

local p_enemies = ParentObject.find("enemies")
local overloading = EliteType.find("Overloading", "Vanilla")

local resources = restre_require("resources/resources")

local COLOR = Color.fromRGB(88, 88, 88)
local COLOR_HARD = Color.fromRGB(44, 44, 44)

local SLOW = 0.32
local HP = 1.4
local THUNDER_CHANCE = 0.08
local RAINDROP_CHANCE = 40

local THUNDER_LIFE = 16
local THUNDER_DISPLACEMENT = 4
local THUNDER_SIZE = 32
local THUNDER_DAMAGE = 32
local THUNDER_LENGTH = 8
local THUNDER_PITCH = 0.2

local wrecked = EliteType.new("Wrecked")
wrecked.displayName = "Wrecked"
wrecked.color = COLOR
wrecked.colorHard = COLOR_HARD
wrecked.palette = resources.sprites.palette

for _,namespace in ipairs(CycloneLib.modloader.getNamespaces()) do
	for _,monster_card in ipairs(MonsterCard.findAll()) do
		--print(monster_card:getName())
		--if monster_card:getName() ~= "Magma Worm" then
			monster_card.eliteTypes:add(wrecked)
		--end
	end
end

local WORM_SPRITE = {
	[Sprite.find("WormHead", "Vanilla")   ] = resources.sprites.wormhead,
	[Sprite.find("WormBody", "Vanilla")   ] = resources.sprites.wormbody,
	[Sprite.find("WormWarning", "Vanilla")] = resources.sprites.wormwarning,
}
local WORM = {
	"Worm",
	"WormHead",
	"WormBody",
}
for i,name in ipairs(WORM) do
	WORM[i] = Object.find(name, "Vanilla")
end
local WORM_LOOK = CycloneLib.table.swap(WORM)
local function worm(i_enemy)
	local o_enemy = i_enemy:getObject()
	if WORM_LOOK[o_enemy] then
		if o_enemy == WORM[2] then
			return Object.findInstance(i_enemy:get("controller") or -1), i_enemy
		else
			return nil, false
		end
	else
		return i_enemy, true
	end
end

local function getCloudPos(i_actor)
	local r_actor = CycloneLib.Rectangle.new(i_actor)
	return r_actor.centerx, r_actor.top - 16, (math.random() - 1/2) * resources.sprites.cloud.width
end

local o_thunder = Object.new("Thunder")

local thunder_packet = net.Packet.new("thunder_create", function(sender, net_enemy, x, y)
	o_thunder:create(x, y)
	if net_enemy then
		local i_enemy = net_enemy:resolve()
		if i_enemy then
			i_enemy:set("wrecked_cloud", THUNDER_LIFE * 2)
		end
	end
end)
local strike_packet = net.Packet.new("thunder_strike", function(sender, net_enemy)
	local i_enemy = net_enemy:resolve()
	if i_enemy then
		if i_enemy:getElite() == nil then
			i_enemy:makeElite(overloading)
		end
	end
end)

o_thunder:addCallback("create", function(i_thunder)
	i_thunder:set("life", THUNDER_LIFE)
	
	if net.host then
		for _,i_enemy in ipairs(p_enemies:findMatching("wrecked", nil)) do
			if (i_enemy:getElite() == nil) and i_enemy:makeElite(overloading) then
				strike_packet:sendAsHost(net.ALL, nil, i_enemy:getNetIdentity())
				break
			end
		end
	end
	
	resources.particles.thunder:burst("above", i_thunder.x, i_thunder.y, 1)
	resources.sounds.thunder:play(1, 1 + (math.random() - 1/2) * THUNDER_PITCH)
	
	local ground = CycloneLib.collision.getGround(i_thunder.x, i_thunder.y)
	local anchor_x = { i_thunder.x }
	local anchor_y = { i_thunder.y }
	local y = i_thunder.y
	local i = 1
	while true do
		local dbase = (math.random() - 1/2) * THUNDER_DISPLACEMENT
		local length = math.min(ground - y, THUNDER_LENGTH)
		anchor_x[i + 1] = anchor_x[i] + dbase
		anchor_y[i + 1] = anchor_y[i] + length
		i = i + 1
		
		y = y + length
		if y >= ground then
			break
		end
		
		local dconnect = (math.random() - 1/2) * THUNDER_DISPLACEMENT
		anchor_x[i + 1] = anchor_x[i] + dconnect
		anchor_y[i + 1] = anchor_y[i]
		i = i + 1
	end
	
	local d_thunder =  i_thunder:getData()
	d_thunder.anchor_x = anchor_x
	d_thunder.anchor_y = anchor_y
	
	misc.fireExplosion(anchor_x[#anchor_x], anchor_y[#anchor_y], THUNDER_SIZE / (19 * 2), THUNDER_SIZE / (4 * 2), THUNDER_DAMAGE, "enemy")
end)
o_thunder:addCallback("step", function(i_thunder)
	i_thunder:set("life", i_thunder:get("life") - 1)
	if i_thunder:get("life") <= 0 then
		i_thunder:destroy()
	end
end)
o_thunder:addCallback("draw", function(i_thunder)
	local d_thunder = i_thunder:getData()
	local anchor_x = d_thunder.anchor_x
	local anchor_y = d_thunder.anchor_y
	
	graphics.color(Color.WHITE) ; graphics.alpha(i_thunder:get("life")/THUNDER_LIFE)
	for i=1,(#anchor_x - 1) do
		graphics.line(anchor_x[i], anchor_y[i], anchor_x[i + 1], anchor_y[i + 1], 1.2)
	end
end)

callback.register("onStep", function()
	for _,i_enemy in ipairs(p_enemies:findAll()) do
		local is_wrecked = i_enemy:getElite() == wrecked
		
		if is_wrecked and WORM_LOOK[i_enemy:getObject()] and WORM_SPRITE[i_enemy.sprite] then
			i_enemy.sprite = WORM_SPRITE[i_enemy.sprite]
		end
		
		local i_enemy, valid = worm(i_enemy)
		
		if valid then
			if is_wrecked then
				if not i_enemy:get("wrecked") then
					i_enemy:set("pHmax", i_enemy:get("pHmax") * (1 - SLOW))
					i_enemy:set("maxhp", i_enemy:get("maxhp") * HP)
					if valid ~= true then
						valid:set("hp", valid:get("hp") * HP)
					else
						i_enemy:set("hp", i_enemy:get("hp") * HP)
					end
					i_enemy:set("wrecked", 1)
				end
				if net.host then
					if (math.random() * 100) < THUNDER_CHANCE then
						local cloud_x, cloud_y, dx = getCloudPos(valid ~= true and valid or i_enemy)
						o_thunder:create(cloud_x + dx, cloud_y)
						i_enemy:set("wrecked_cloud", THUNDER_LIFE * 2)
						local status, net_enemy = pcall(i_enemy.getNetIdentity, i_enemy)
						if not status then net_enemy = nil end
						thunder_packet:sendAsHost(net.ALL, nil, net_enemy, cloud_x + dx, cloud_y)
					end
				end
			else
				if i_enemy:get("wrecked") then
					i_enemy:set("pHmax", i_enemy:get("pHmax") / (1 - SLOW))
					i_enemy:set("maxhp", i_enemy:get("maxhp") / HP)
					if valid ~= true then
						valid:set("hp", valid:get("hp") / HP)
					else
						i_enemy:set("hp", i_enemy:get("hp") / HP)
					end
					i_enemy:set("wrecked", nil)
				end
			end
		end
	end
end)

callback.register("onGameStart", function()
	graphics.bindDepth(2, function()
		local any_wrecked
		for _,i_enemy in ipairs(p_enemies:findAll()) do
			local is_wrecked = i_enemy:getElite() == wrecked
			
			local i_enemy, valid = worm(i_enemy)
			
			if valid then
				if is_wrecked then
					any_wrecked = true
					
					local cloud_x, cloud_y, dx = getCloudPos(valid ~= true and valid or i_enemy)
					
					local thunder
					local cloud_life = i_enemy:get("wrecked_cloud")
					if cloud_life then
						if cloud_life <= 0 then
							i_enemy:set("wrecked_cloud", nil)
						else
							i_enemy:set("wrecked_cloud", cloud_life - 1)
						end
						thunder = true
					end
					
					graphics.drawImage{
						image = resources.sprites.cloud,
						x = cloud_x,
						y = cloud_y,
						subimage = (thunder ~= nil) and 2 or 1,
					}
					
					if math.chance(RAINDROP_CHANCE) then
						resources.particles.raindrop:burst("above", cloud_x + dx, cloud_y + 16, 1)
					end
				end
			end
		end
		
		local s_rain = resources.sounds.rain
		if any_wrecked then	
			if not s_rain:isPlaying() then
				s_rain:play(0.4, 1)
			end
		elseif s_rain:isPlaying() then
			s_rain:stop()
		end
	end):set("persistent", 1)
end)