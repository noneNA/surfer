-- Surfer - contents/elites/wrecked/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}
resources.particles = {}
resources.sounds = {}

resources.sprites.palette = restre_spriteLoad("palette.png", 1, 0, 0)
resources.sprites.cloud = restre_spriteLoad("cloud.png", 2, 16, 16)

local s_wormhead = Sprite.find("WormHead", "Vanilla")
local s_wormbody = Sprite.find("WormBody", "Vanilla")
local s_wormwarning = Sprite.find("WormWarning", "Vanilla")
resources.sprites.wormhead = restre_spriteLoad("wormhead.png", 6, s_wormhead.xorigin, s_wormhead.yorigin)
resources.sprites.wormbody = restre_spriteLoad("wormbody.png", 3, s_wormbody.xorigin, s_wormbody.yorigin)
resources.sprites.wormwarning = restre_spriteLoad("wormwarning.png", 6, s_wormwarning.xorigin, s_wormwarning.yorigin)

resources.sounds.thunder = restre_soundLoad("thunder.ogg")
resources.sounds.rain = restre_soundLoad("rain.ogg")

local raindrop = ParticleType.new("RainDrop")
raindrop:shape("line")
raindrop:color(Color.fromRGB(64, 64, 64))
--raindrop:color(Color.fromRGB(255, 255, 255))
raindrop:alpha(0, 0.64, 0)
raindrop:additive(true)
raindrop:scale(0.4, 0.1)
raindrop:angle(90, 90, 0, 0, false)
raindrop:speed(0.8, 0.8, 0, 0)
raindrop:direction(270 - 30, 270 + 30, 0, 0)
raindrop:gravity(0.08, 270)
raindrop:life(16, 16)
resources.particles.raindrop = raindrop

local thunder = ParticleType.new("Thunder")
thunder:shape("sphere")
thunder:color(Color.WHITE)
thunder:alpha(0.08, 0.16, 0)
thunder:scale(64, 64)
thunder:life(8, 8)
resources.particles.thunder = thunder

return resources