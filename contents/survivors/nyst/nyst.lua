-- Surfer -- contents/survivors/nyst/nyst.lua

-- Dependencies:
---- CycloneLib.Projectile
---- CycloneLib.Rectangle

local resources = restre_require("resources/resources")

--local COLOR = Color.fromRGB(64,128,255)
local COLOR = Color.fromRGB(64,255,128)

local HP = 125
local DAMAGE = 16
local REGEN = 0.02

local HP_LEVEL = 16
local DAMAGE_LEVEL = 4
local REGEN_LEVEL = 0.001
local ARMOR_LEVEL = 4

local SNOWBALL_DAMAGE = 1.2
local SNOWBALL_SIZE = 10
local SNOWBALL_LIFE = 400
local SNOWBALL_VX = 4
local SNOWBALL_VY = -4*10^(-1)
local SNOWBALL_AY = 4*10^(-2)
local SNOWBALL_DISTANCE = 8

local GATHER_DAMAGE = 1.8
local GATHER_SIZE = 80
--local GATHER_DISTANCE = 8
local COLD_DURATION = 240
local COLD_SLOW = 0.4
local COLD_COLOR = Color.fromRGB(100,200,255)

local SNOWMAN_SPEED = 0.6
local SNOWMAN_INVINCIBLE = 10
local SNOWMAN_PARTICLE_AMOUNT = 2
local SNOWMAN_PARTICLE_CHANCE = 16
local SNOWMAN_PARTICLE_DISTANCE = 4

local AVALANCHE_PLAYER_SPEED = 2
local AVALANCHE_OFFSET = 8
local AVALANCHE_GRAVITY = 0.1
local AVALANCHE_SPEED = 0.8
local AVALANCHE_ACCELERATION = 0.01
local AVALANCHE_DAMAGE = 1.6
local AVALANCHE_SNOWBALL = 0.1
local AVALANCHE_LIFE = 300
local AVALANCHE_STUN = 0.4
local AVALANCHE_KNOCKBACK = 2.4

local SKILL_ACTIVITY = {
	[1] = { 1/4, true, true },
	[2] = { 1/4, true, true },
	[3] = { 1/120, false, true },
	[4] = { 1/8, true, true },
}

local SKILL_INFO = {
	NAME = {
		[1] = "Snowball",
		[2] = "Gather Snow",
		[3] = "Freeze",
		[4] = "Avalanche",
		[5] = "Rolling Winter",
	},
	DESCRIPTION = {
		[1] = string.format(
			"Throw a &lt&snowball&!& that deals &or&%s%%&!& damage.",
			SNOWBALL_DAMAGE * 100
		),
		[2] = string.format(
			"Enhance your next &lt&snowball&!& for &or&%s%%&!& more damage, more area and &y&%ss&!& slow.\n"
			.."Subsequent activations have dimnishing effects.",
			(GATHER_DAMAGE - SNOWBALL_DAMAGE) * 100,
			COLD_DURATION / 60
		),
		[3] = string.format(
			"Become a &lt&snowman&!& for &y&%s&!& seconds\n"
			.."You are &y&slowed&!& but &b&invulnerable&!& for the duration.",
			((resources.sprites.skill_3.frames - 1) / SKILL_ACTIVITY[3][1]) / 60
		),
		[4] = string.format(
			"Roll some &lt&snow&!& to make a &lt&giant snowball&!& that hits enemies for &or&%s%%&!& damage.\n"
			.."Every hit increases the damage by &or&%s%%&!&.",
			AVALANCHE_DAMAGE * 100,
			AVALANCHE_SNOWBALL * 100
		),
		[5] = string.format(
			"Make a giant snowball that hits for &or&%s%%&!& and slows for &y&%s&!& seconds.\n"
			.."Every hit increases the damage by &or&%s%%&!&.",
			AVALANCHE_DAMAGE * 100,
			COLD_DURATION / 60,
			AVALANCHE_SNOWBALL * 100
		),
	},
	COOLDOWN = {
		[1] = 40,
		[2] = 240,
		[3] = 320,
		[4] = 300,
		[5] = 300,
	}
}

local animations = {
	idle  = resources.sprites.idle,
	walk  = resources.sprites.walk,
	jump  = resources.sprites.jump,
	climb = resources.sprites.climb,
	death = resources.sprites.death,
	decoy = resources.sprites.decoy,
}

local p_enemies = ParentObject.find("enemies")
local p_actors = ParentObject.find("actors")
local s_pmask = Sprite.find("PMask")

local cold = Buff.new("Cold")
cold.sprite = resources.sprites.cold
cold:addCallback("start", function(instance)
	instance:set("pHmax", instance:get("pHmax") - COLD_SLOW)
end)
cold:addCallback("end", function(instance)
	instance:set("pHmax", instance:get("pHmax") + COLD_SLOW)
end)
callback.register("onDraw", function()
	for _,i_actor in ipairs(p_actors:findAll()) do
		if i_actor:hasBuff(cold) then
			graphics.drawImage{
				image = i_actor.sprite or i_actor.mask,
				x = i_actor.x,
				y = i_actor.y,
				subimage = i_actor.subimage,
				color = COLD_COLOR,
				alpha = 1,
				angle = i_actor.angle,
				xscale = i_actor.xscale,
				yscale = i_actor.yscale,
			}
		end
	end
end)

local o_snowball = CycloneLib.Projectile.new({
	name = "Snowball",
	sprite = resources.sprites.snowball,
	vx = SNOWBALL_VX,
	vy = SNOWBALL_VY,
	ay = SNOWBALL_AY,
	explosion = 1,
	impact_explosion = 1,
	explosionw = SNOWBALL_SIZE,
	explosionh = SNOWBALL_SIZE,
	damage = SNOWBALL_DAMAGE,
	--deathsprite_hit = resources.sprites.snowball_hit
	life = SNOWBALL_LIFE,
})
o_snowball:addCallback("step", function(i_snowball)
	if i_snowball:isValid() and i_snowball:get("Projectile_dead") == 0 then
		local distance = (i_snowball:get("gather") == 1) and SNOWBALL_DISTANCE or 0
		resources.particles.snowball:burst(
			"below",
			i_snowball.x + (math.random() - 1/2) * distance,
			i_snowball.y + (math.random() - 1/2) * distance,
			1
		)
	end
end)
CycloneLib.Projectile.addCallback(o_snowball, "preHit", function()
	local s_hit = resources.sounds.hit
	if not s_hit:isPlaying() then
		s_hit:play(1, 0.32)
	end
end)

local o_avalanche = Object.new("Avalanche")
o_avalanche.sprite = resources.sprites.avalanche
o_avalanche:addCallback("create", function(i_avalanche)
	i_avalanche.mask = s_pmask
	i_avalanche:getData().hits = {}
	i_avalanche:set("snowball", 0)
	i_avalanche:set("vx", 0)
	i_avalanche:set("vy", 0)
	i_avalanche:set("ax", 0)
	i_avalanche:set("ay", AVALANCHE_GRAVITY)
	i_avalanche:set("life", AVALANCHE_LIFE)
end)
o_avalanche:addCallback("step", function(i_avalanche)
	i_avalanche:set("life", i_avalanche:get("life") - 1)
	if i_avalanche:get("life") <= 0 then
		i_avalanche:destroy()
		return nil
	end
	
	i_avalanche:set("vx", i_avalanche:get("vx") + i_avalanche:get("ax"))
	i_avalanche:set("vy", i_avalanche:get("vy") + i_avalanche:get("ay"))
	
	local vx, vy = i_avalanche:get("vx") or 0, i_avalanche:get("vy") or 0
	local radius = resources.sprites.avalanche.width/2
	local frames = resources.sprites.avalanche.frames
	i_avalanche.spriteSpeed = frames * (math.abs(vx) / (2 * math.pi * radius))
	
	local dx, dy = vx, vy
	local sx, sy = math.sign(dx), math.sign(dy)
	repeat
		local x, y = sx * math.min(math.abs(dx), 1), sy * math.min(math.abs(dy), 1)
		if not i_avalanche:collidesMap(i_avalanche.x + x, i_avalanche.y + y) then
			i_avalanche.x = i_avalanche.x + x
			i_avalanche.y = i_avalanche.y + y
			dx = dx - x
			dy = dy - y
		else
			if i_avalanche:collidesMap(i_avalanche.x + x, i_avalanche.y) then
				dx = 0
				i_avalanche:set("vx", 0)
			end
			if i_avalanche:collidesMap(i_avalanche.x, i_avalanche.y + y) then
				dy = 0
				i_avalanche:set("vy", 0)
			end
		end
		if math.sign(dx) ~= sx then dx = 0 end
		if math.sign(dy) ~= sy then dy = 0 end
	until (dx == 0) and (dy == 0)
		
	local d_avalanche = i_avalanche:getData()
	local hits = d_avalanche.hits
	local parent = Object.findInstance(i_avalanche:get("parent") or -1)
	if not parent then return nil end
	local scepter = parent:get("scepter") > 0
	for _,i_enemy in ipairs(p_enemies:findAll()) do
		if (not hits[i_enemy]) and i_avalanche:collidesWith(i_enemy, i_avalanche.x, i_avalanche.y) then
			local damager = parent:fireBullet(i_enemy.x - i_avalanche.xscale * 16, i_enemy.y, i_avalanche.xscale == -1 and 180 or 0, 16, AVALANCHE_DAMAGE + i_avalanche:get("snowball") * AVALANCHE_SNOWBALL)
			damager:set("specific_target", i_enemy.id)
			damager:set("stun", AVALANCHE_STUN)
			damager:set("knockback", AVALANCHE_KNOCKBACK)
			i_avalanche:set("snowball", i_avalanche:get("snowball") + 1)
			if scepter then
				i_enemy:applyBuff(cold, COLD_DURATION)
			end
			hits[i_enemy] = true
			local s_hit = resources.sounds.hit
			if not s_hit:isPlaying() then
				s_hit:play(1, 0.32)
			end
		end
	end
end)

local nyst = Survivor.new("Nyst")
nyst.displayName = "Nyst"
nyst.titleSprite = resources.sprites.title
nyst.idleSprite = resources.sprites.idle
nyst.loadoutSprite = resources.sprites.loadout
nyst.loadoutColor = COLOR
nyst.endingQuote = "...ending quote..."

nyst:setLoadoutInfo(
	string.format(
		"&g&%s&!& is a survivor capable of high amounts of damage.\n"
		.."&g&%s&!& gives his &g&%s&!&s great damage and crowd control.\n"
		.."He can also crawl his way out of danger with &g&%s&!&\n"
		.."while &g&%s&!& continues to deal with crowds.",
		nyst.displayName,
		SKILL_INFO.NAME[2],
		SKILL_INFO.NAME[1],
		SKILL_INFO.NAME[3],
		SKILL_INFO.NAME[4]
	),
	resources.sprites.skill_icons
)

for i=1,4 do
	nyst:setLoadoutSkill(
		i,
		SKILL_INFO.NAME[i],
		SKILL_INFO.DESCRIPTION[i]
	)
end

local function setSkill(player, i)
	player:setSkill(
		math.min(i, 4),
		SKILL_INFO.NAME[i],
		SKILL_INFO.DESCRIPTION[i]:gsub("%&[^&]*%&",""),
		resources.sprites.skill_icons,
		i,
		SKILL_INFO.COOLDOWN[i]
	)	
end

nyst:addCallback("init", function(player)
	player:setAnimations(animations)
	player:survivorSetInitialStats(HP, DAMAGE, REGEN)
	
	for i=1,4 do
		setSkill(player, i)
	end
end)

nyst:addCallback("useSkill", function(player, skill)
	if (player:get("activity") == 0) then
		if skill == 3 then
			local left = player:control("left") ~= input.NEUTRAL
			local right = player:control("right") ~= input.NEUTRAL
			player:set("snowman", 1)
			player:set("snowman_direction", (left or right) and player.xscale or 0)
			resources.sounds.snowman:play(1, 1.2)
		end
		player:survivorActivityState(skill,
			resources.sprites[string.format("skill_%s", skill)],
			SKILL_ACTIVITY[skill][1],
			SKILL_ACTIVITY[skill][2],
			SKILL_ACTIVITY[skill][3]
		)
		player:activateSkillCooldown(skill)
	end
end)

nyst:addCallback("onSkill", function(player, skill, frame)
	if skill == 1 then
		if frame == 4 then
			for i=0,player:get("sp") do
				local i_snowball = CycloneLib.Projectile.fire(o_snowball, player.x + player.xscale * 4, player.y - 6, player)
				local gather = player:get("gather_snow") or 0
				if gather >= 1 then
					CycloneLib.Projectile.configure(i_snowball, {
						damage = GATHER_DAMAGE + (GATHER_DAMAGE - SNOWBALL_DAMAGE) * (gather - 1) / 2,
						damager_variables = { snowball_slow = 1 , climb = i * 8 },
						explosionw = GATHER_SIZE,
						explosionh = GATHER_SIZE,
					})
					i_snowball:set("gather", 1)
				else
					CycloneLib.Projectile.configure(i_snowball, {
						damager_variables = { climb = i * 8 }
					})
				end
			end
			player:set("gather_snow", 0)
			resources.sounds.throw:play(1, 1)
		end
		if frame >= resources.sprites.skill_1.frames - 3 then
			player:set("pHspeed", player.xscale)
		end
	elseif skill == 2 then
		if frame == 1 then
			resources.sounds.gather:play(1.2, 1)
		elseif frame == resources.sprites.skill_2.frames - 1 then
			player:set("gather_snow", (player:get("gather_snow") or 0) + 1)
			--[[
			local v_player = CycloneLib.Vector2.new(player)
			local v_gather = CycloneLib.Vector2.new(GATHER_DISTANCE, 0)
			for angle = 0, 360, 30 do
				v_gather.angle = angle
				local v_particle = v_player + v_gather
				local particle = resources.particles.gather
				particle:burst("above", v_particle.x, v_particle.y, 1)
			end
			--]]
		end
	elseif skill == 3 then
		if frame == resources.sprites.skill_2.frames - 1 then
			if player:get("invincible") <= SNOWMAN_INVINCIBLE then
				player:set("invincible", 0)
			end
		else
			if player:get("invincible") < SNOWMAN_INVINCIBLE then
				player:set("invincible", SNOWMAN_INVINCIBLE)
			end
		end
		local left = player:control("left")
		local right = player:control("right")
		if left == input.PRESSED then player:set("snowman_direction", -1) ; player.xscale = -1 end
		if right == input.PRESSED then player:set("snowman_direction", 1) ; player.xscale = 1 end
		if (left == input.NEUTRAL) and (right == input.NEUTRAL) then
			player:set("snowman_direction", 0)
		end
		player:set("pHspeed", player:get("snowman_direction") * player:get("pHmax") * SNOWMAN_SPEED)

		local r_player = CycloneLib.Rectangle.new(player)
		if frame == 1 then
			for i=1,SNOWMAN_PARTICLE_AMOUNT * SNOWMAN_PARTICLE_AMOUNT do
				resources.particles.snowman:burst(
					"below",
					r_player.centerx + (math.random() - 1/2) * r_player.h,
					r_player.centery + (math.random() - 1/2) * r_player.h,
					SNOWMAN_PARTICLE_AMOUNT
				)
			end
		end
		if math.chance(SNOWMAN_PARTICLE_CHANCE) then
			resources.particles.snowman:burst(
				"below",
				r_player.centerx,
				r_player.bottom - math.random() * SNOWMAN_PARTICLE_DISTANCE,
				SNOWMAN_PARTICLE_AMOUNT
			)
		end
	elseif skill == 4 then
		if (frame ~= 0) and (player:get("free") == 0) then
			player:set("pHspeed", player.xscale * AVALANCHE_PLAYER_SPEED)
		end
		if (frame == 1) or ((frame == resources.sprites.skill_4.frames - 1) and (player:get("sp") > 0)) then
			local i_avalanche = o_avalanche:create(player.x + player.xscale * AVALANCHE_OFFSET, player.y)
			i_avalanche:set("vx", player.xscale * AVALANCHE_SPEED)
			i_avalanche:set("ax", player.xscale * AVALANCHE_ACCELERATION)
			i_avalanche:set("parent", player.id)
			i_avalanche.xscale = player.xscale
		end
	end
end)

callback.register("onHit", function(damager, i_hit)
	if damager:get("snowball_slow") then
		i_hit:applyBuff(cold, COLD_DURATION)
	end
end)

nyst:addCallback("levelUp", function(player)
	player:survivorLevelUpStats(HP_LEVEL, DAMAGE_LEVEL, REGEN_LEVEL, ARMOR_LEVEL)
end)

nyst:addCallback("scepter", function(player)
	setSkill(player, 5)
end)