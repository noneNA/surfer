-- Surfer -- contents/survivors/nyst/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}
resources.particles = {}
resources.sounds = {}

resources.sprites.snowball = restre_spriteLoad("snowball.png", 1, 4, 4)

resources.sprites.cold = restre_spriteLoad("cold.png", 1, 4, 4)

resources.sprites.avalanche = restre_spriteLoad("avalanche.png", 4, 8, 8)

resources.sprites.title = restre_spriteLoad("title.png", 12, 6, 10)
resources.sprites.loadout = restre_spriteLoad("loadout.png", 17, 0, 0)
resources.sprites.skill_icons = restre_spriteLoad("skill_icons.png", 5, 0, 0)

resources.sprites.idle  = restre_spriteLoad("idle.png", 1, 6, 8)
resources.sprites.walk  = restre_spriteLoad("walk.png", 12, 6, 10)
resources.sprites.jump  = restre_spriteLoad("jump.png", 1, 8, 8)
resources.sprites.climb = restre_spriteLoad("climb.png", 2, 8, 8)
resources.sprites.death = restre_spriteLoad("death.png", 3, 8, 8)
resources.sprites.decoy = restre_spriteLoad("decoy.png", 1, 10, 18)

resources.sprites.skill_1 = restre_spriteLoad("skill_1.png", 6, 6, 8)
resources.sprites.skill_2 = restre_spriteLoad("skill_2.png", 4, 6, 8)
resources.sprites.skill_3 = restre_spriteLoad("skill_3.png", 2, 7, 8)
resources.sprites.skill_4 = restre_spriteLoad("skill_4.png", 3, 6, 8)

local snowman = ParticleType.new("Snowman")
snowman:life(64, 64)
snowman:shape("pixel")
snowman:color(Color.WHITE)
snowman:alpha(1, 0.64)
snowman:size(0.24, 0.24, 0, 0)
snowman:speed(0.04, 0.08, 0, 0)
snowman:direction(90, 90, 0, 0)
resources.particles.snowman = snowman

local snowball = ParticleType.new("Snowball")
snowball:life(16, 16)
snowball:shape("pixel")
snowball:color(Color.WHITE)
snowball:alpha(1, 0.8, 0)
snowball:size(0.8, 0.8, 0, 0)
snowball:speed(0.04, 0.08, 0, 0)
snowball:direction(0,360,0,16)
resources.particles.snowball = snowball

--[[
local gather = ParticleType.new("gather")
gather:life(16, 16)
gather:shape("pixel")
gather:color(Color.WHITE)
gather:alpha(1, 0.8, 0)
--gather:speed(0.8, 0.8, 0, 0.02)
--gather:direction(270, 270, 0, 30)
resources.particles.gather = gather
--]]

resources.sounds.hit = restre_soundLoad("hit.ogg")
resources.sounds.throw = restre_soundLoad("throw.ogg")

resources.sounds.gather = restre_soundLoad("gather.ogg")

resources.sounds.snowman = restre_soundLoad("snowman.ogg")

return resources