-- Surfer - contents/survivors/surfer/surfer.lua

-- Dependencies:
---- CycloneLib

local resources = restre_require("resources/resources")

local COLOR = Color.fromRGB(0,255,255)

local MAX_WET = 32

local CLIMB = 16

local HP = 120
local DAMAGE = 12
local REGEN = 0.02

local HP_LEVEL = 16
local DAMAGE_LEVEL = 4
local REGEN_LEVEL = 0.002
local ARMOR_LEVEL = 4

local SPIN_DAMAGE = 0.8
local SPIN_WET = 120
local SPIN_WIDTH = 64
local SPIN_HEIGHT = 40
local SPIN_MOVE_SPEED = 1

local GLIMPSE_CORE = 16
local GLIMPSE_DISTANCE = 400
local GLIMPSE_BASE = 1.2
local GLIMPSE_WET = 0.1
local GLIMPSE_INVINCIBLE = 2

local WAVE_LIFE = 180
local WAVE_WIDTH = 4
local WAVE_SPEED = 1.2
local WAVE_WET = 300
local SURF_JUMP = 3.2
local WAVE_TRAIL = 8
local WAVE_WAVE = 1.8

local OCEANS_CALL_REDUCTION = 1 / MAX_WET
local OCEANS_CALL_REDUCTION_BASE = 1/2
local OCEANS_CALL_DURATION_BASE = (8 * 60) / 4
local OCEANS_CALL_DURATION_WET = OCEANS_CALL_DURATION_BASE / MAX_WET
local OCEANS_CALL_RADIUS = 480
local ABSORB_DISTANCE = 8
local ABSORB_SPEED = 4
local ABSORB_CATCH = 0.02
local ABSORB_PARTICLE_CHANCE = 86
local ABSORB_PARTICLE_DISTANCE = 2

local DEATH_SPRITESPEED = 1/2

local SKILL_ACTIVITY = {
	[1] = { 1/4, true, false },
	[2] = { 0.8, true, true },
	[3] = { 000, true, true },
	[4] = { 1/4, true, true },
}

local SKILL_INFO = {
	NAME = {
		[1] = "360",
		[2] = "Glimpse",
		[3] = "Surf",
		[4] = "Ocean's Call",
		[5] = "Ocean's Echo",
	},
	DESCRIPTION = {
		[1] = string.format(
			"Spin your board for &or&2x%s%%&!& damage.\n"
			.."&b&Wets&!& enemies for &y&%s&!& seconds.",
			SPIN_DAMAGE * 100,
			SPIN_WET / 60
		),
		[2] = string.format(
			"Shortly disappear in order to deal &or&%s%%&!& damage to both sides.\n"
			--.."&lt&Dries&!& &b&wet&!& enemies to increase its damage by &or&%s%%&!&.",
			.."Each &b&wet&!& enemy hit increases its damage by &or&%s%%&!&.",
			GLIMPSE_BASE * 100,
			GLIMPSE_WET * 100
		),
		[3] = string.format(
			"Call a &b&cosmic wave&!& to surf upon or surf an already existing one.\n"
			.."The wave &b&wets&!& enemies for &y&%s&!& seconds.",
			WAVE_WET / 60
		),
		[4] = string.format(
			--"&lt&Absorb&!& nearby &b&wetness&!& to gain &g&%s%% damage reduction&!& plus &g&%s%%&!& for every &lt&absorbtion&!&.\n"
			--.."Lasts for &y&%s&!& seconds plus &y&%s&!& more for every &lt&absorbtion&!&.",
			"Grants &g&%s%% damage reduction&!& plus &g&%s%%&!& for every &b&wet&!& enemy nearby.\n"
			.."Lasts for &y&%s&!& seconds plus &y&%s&!& more for &b&wet&!& enemy nearby.",
			OCEANS_CALL_REDUCTION_BASE * 100,
			(1 - OCEANS_CALL_REDUCTION_BASE) * OCEANS_CALL_REDUCTION * 100,
			OCEANS_CALL_DURATION_BASE / 60,
			OCEANS_CALL_DURATION_WET / 60
		),
		[5] = string.format(
			--"&lt&Absorb&!& nearby &b&wetness&!& to gain &g&100%% damage reduction&!&.\n"
			--.."Lasts for &y&%s&!& seconds plus &y&%s&!& more for every &lt&absorbtion&!&.",
			"Grants &g&100%% damage reduction&!&.\n"
			.."Lasts for &y&%s&!& seconds plus &y&%s&!& more for every &b&wet&!& enemy nearby.",
			OCEANS_CALL_REDUCTION * 100,
			OCEANS_CALL_DURATION_BASE / 60,
			OCEANS_CALL_DURATION_WET / 60
		),
	},
	COOLDOWN = {
		[1] = 40,
		[2] = 2 * 60,
		[3] = 4 * 60,
		[4] = 8 * 60,
		[5] = 8 * 60,
	}
}

local animations = {
	idle  = resources.sprites.idle,
	walk  = resources.sprites.walk,
	jump  = resources.sprites.jump,
	climb = resources.sprites.climb,
	death = resources.sprites.death,
	decoy = resources.sprites.decoy,
}

local p_enemies = ParentObject.find("enemies")

local wet = Buff.new("Wet")
wet.sprite = resources.sprites.wet

local o_wave = Object.new("Wave")
--o_wave.sprite = resources.sprites.wave_mask
o_wave:addCallback("create", function(i_wave)
	i_wave.mask = resources.sprites.wave_mask
	i_wave.xscale = WAVE_WIDTH
	i_wave.yscale = 1
	i_wave:set("life", WAVE_LIFE)
end)
o_wave:addCallback("step", function(i_wave)
	local life = i_wave:get("life")
	if life <= 0 then
		i_wave:destroy()
		return nil
	end
	local old_width = math.abs(i_wave.xscale * i_wave.mask.width)
	local direction = math.sign(i_wave.xscale)
	i_wave:set("hspeed", WAVE_SPEED * direction)
	i_wave.xscale = WAVE_WIDTH * (life/WAVE_LIFE) * direction
	i_wave:set("life", life - 1)
	local new_width = math.abs(i_wave.xscale * i_wave.mask.width)
	i_wave.x = i_wave.x + direction * (old_width - new_width) / 2
	local r_wave = CycloneLib.Rectangle.new(i_wave)
	for _,i_enemy in ipairs(p_enemies:findAllRectangle(r_wave.left, r_wave.top, r_wave.right, r_wave.bottom)) do
		if not i_enemy:hasBuff(wet) then
			i_enemy:applyBuff(wet, WAVE_WET)
		end
	end
end)
o_wave:addCallback("draw", function(i_wave)
	local life = i_wave:get("life")
	local _xscale, width, xorigin = i_wave.xscale, i_wave.mask.width, i_wave.mask.xorigin
	local direction = math.sign(_xscale)
	local size = math.abs(_xscale)
	local step = math.floor(((size > 1) and ((size - 1) * width) or 0)/WAVE_TRAIL) + 1
	for i=1,step do
		graphics.drawImage{
			x = i_wave.x + direction * (i - step) * WAVE_TRAIL + _xscale * (width - xorigin),
			y = i_wave.y + (i/step) * (life/WAVE_LIFE) * WAVE_WAVE * math.sin(i_wave.x/4 + i),
			image = resources.sprites.wave,
			xscale = direction,
			alpha = ((size > 1) and 1 or (life/WAVE_LIFE)) * (i/step),
		}
	end
end)

local o_absorb = Object.new("Absorb")
--[[
o_absorb.sprite = resources.sprites.absorb
o_absorb:addCallback("create", function(i_absorb)
	i_absorb.alpha = 1/2
end)
--]]
o_absorb:addCallback("step", function(i_absorb)
	local i_target = Object.findInstance(i_absorb:get("target"))
	local dx, dy = i_target.x - i_absorb.x, i_target.y - i_absorb.y
	if math.sqrt(dx^2 + dy^2) <= ABSORB_DISTANCE then
		resources.sounds.absorb:play(1 + (math.random() - 1/2) * 0.4, 1)
		i_absorb:destroy()
	else
		local angle = math.deg(math.atan2(-dy,dx))
		i_absorb:set("speed", ABSORB_SPEED)
		i_absorb:set("direction", angle)
		i_absorb.x = i_absorb.x + dx * ABSORB_CATCH
		i_absorb.y = i_absorb.y + dy * ABSORB_CATCH
		--i_absorb.angle = angle + 90
	end
end)
o_absorb:addCallback("draw", function(i_absorb)
	if math.chance(ABSORB_PARTICLE_CHANCE) then
		resources.particles.absorb:burst(
			"below",
			i_absorb.x + (math.random() - 1/2) * ABSORB_PARTICLE_DISTANCE,
			i_absorb.y + (math.random() - 1/2) * ABSORB_PARTICLE_DISTANCE,
			1
		)
	end
end)

local function findWave(player)
	local r_player = CycloneLib.Rectangle.new(player)
	return o_wave:findRectangle(r_player.left, r_player.top, r_player.right, r_player.bottom)
end

local function enterSurf(player)
	player:set("surfing", 1)
	--player:set("restore_jumps", player:get("feather") - player:get("jump_count"))
	player:setAnimation("jump", resources.sprites.surf)
end

local function exitSurf(player, jump, cancel)
	if jump then
		player:set("pVspeed", -SURF_JUMP)
		local jump_count = player:get("jump_count")
		if jump_count < player:get("feather") then
			player:set("jump_count", jump_count - 1)
		end
	end
	if cancel then
		player:set("block_surf_held", 1)
	end
	player:set("surfing", nil)
	player:setAnimation("jump", animations.jump)
end

local surfer = Survivor.new("Surfer")
surfer.displayName = "Surfer"
surfer.titleSprite = resources.sprites.title
surfer.idleSprite = resources.sprites.idle
surfer.loadoutSprite = resources.sprites.loadout
surfer.loadoutColor = COLOR
surfer.endingQuote = "...and so he left, drifting endlessly through the cosmos..."

surfer:setLoadoutInfo(
	string.format(
		"The &b&Surfer&!& is a survivor that shines when outnumbered.\n"
		.."Use &b&%s&!& to reduce incoming damage\n"
		.."and deal high damage to crowds using &b&%s&!&.\n"
		.."If you ever find yourself in danger use &b&%s&!& and evacuate.",
		SKILL_INFO.NAME[4],
		SKILL_INFO.NAME[2],
		SKILL_INFO.NAME[3]
	),
	resources.sprites.skill_icons
)

for i=1,4 do
	surfer:setLoadoutSkill(
		i,
		SKILL_INFO.NAME[i],
		SKILL_INFO.DESCRIPTION[i]
	)
end

local function setSkill(player, i)
	player:setSkill(
		math.min(i, 4),
		SKILL_INFO.NAME[i],
		SKILL_INFO.DESCRIPTION[i]:gsub("%&[^&]*%&",""),
		resources.sprites.skill_icons,
		i,
		SKILL_INFO.COOLDOWN[i]
	)	
end

surfer:addCallback("init", function(player)
	player:setAnimations(animations)
	player:survivorSetInitialStats(HP, DAMAGE, REGEN)
	
	for i=1,4 do
		setSkill(player, i)
	end
end)

surfer:addCallback("useSkill", function(player, skill)
	if player:get("activity") == 0 then
		if skill <= 2 then
			player:survivorActivityState(skill,
				resources.sprites[string.format("skill_%s", skill)],
				SKILL_ACTIVITY[skill][1],
				SKILL_ACTIVITY[skill][2],
				SKILL_ACTIVITY[skill][3]
			)
			player:activateSkillCooldown(skill)
			if skill == 1 then
				local left = player:control("left") ~= input.NEUTRAL
				local right = player:control("right") ~= input.NEUTRAL
				player:set("spin_direction", (left or right) and player.xscale or 0)
			end
		elseif skill == 3 then
			if player:get("surfing") ~= 1 then
				local i_wave = findWave(player)
				if not i_wave then
					local wave_width = (WAVE_WIDTH * resources.sprites.wave.width)
					local i_wave = o_wave:create(player.x - player.xscale * wave_width/4, player.y)
					i_wave.xscale = math.abs(i_wave.xscale) * player.xscale
					player:activateSkillCooldown(skill)
				end
				enterSurf(player)
			end
		else
			local wet_count = 0
			local enemies = p_enemies:findAllEllipse(
				player.x - OCEANS_CALL_RADIUS,
				player.y - OCEANS_CALL_RADIUS,
				player.x + OCEANS_CALL_RADIUS,
				player.y + OCEANS_CALL_RADIUS
			)
			for _,i_enemy in ipairs(enemies) do
				if i_enemy:hasBuff(wet) then
					wet_count = wet_count + 1
					--i_enemy:removeBuff(wet)
					local sprite = i_enemy.sprite or i_enemy.mask
					local i_absorb = o_absorb:create(i_enemy.x, i_enemy.y - sprite.yorigin - 8)
					i_absorb:set("target", player.id)
				end
				if wet_count >= MAX_WET then
					break
				end
			end
			player:set("oceans_call", (player:get("scepter") < 1) and OCEANS_CALL_REDUCTION_BASE + (1 - OCEANS_CALL_REDUCTION_BASE) * OCEANS_CALL_REDUCTION * wet_count or 1)
			player:set("oceans_call_duration", OCEANS_CALL_DURATION_BASE + OCEANS_CALL_DURATION_WET * wet_count)
			player:activateSkillCooldown(skill)
		end
	end
end)

surfer:addCallback("onSkill", function(player, skill, frame)
	if skill == 1 then
		if player:control("right") == input.PRESSED then player:set("spin_direction", 1)  end
		if player:control("left")  == input.PRESSED then player:set("spin_direction", -1) end
		player:set("pHspeed", player:get("spin_direction") * player:get("pHmax") * SPIN_MOVE_SPEED)
		if (frame == 2) or (frame == 5) then
			for sp=0,player:get("sp") do
				local damager = player:survivorFireHeavenCracker(SPIN_DAMAGE)
				damager = damager or player:fireExplosion(
					player.x,
					player.y,
					SPIN_WIDTH/(19*2),
					SPIN_HEIGHT/(4*2),
					SPIN_DAMAGE,
					nil,
					nil,
					nil
				)
				damager:set("wet", 1):set("climb", sp * CLIMB)
			end
			if frame == 2 then
				resources.sounds.spin:play(1 + (math.random() - 1/2) * 0.4, 6)
			end
		end
	elseif skill == 2 then
		player:set("pVspeed", -player:get("pGravity1"))
		player:set("pHspeed", 0)
		if frame == resources.sprites.skill_2.frames - 1 then
			if player:get("invincible") <= GLIMPSE_INVINCIBLE then
				player:set("invincible", 0)
			end
		else
			if player:get("invincible") < GLIMPSE_INVINCIBLE then
				player:set("invincible", GLIMPSE_INVINCIBLE)
			end
		end
		if frame == 3 then
			local range = GLIMPSE_DISTANCE - GLIMPSE_CORE
			local wet_count = 0
			for _,i_enemy in ipairs(p_enemies:findAllLine(player.x - range, player.y, player.x + range, player.y)) do
				if i_enemy:hasBuff(wet) then
					wet_count = wet_count + 1
					--i_enemy:removeBuff(wet)
				end
				if wet_count >= MAX_WET then
					break
				end
			end
			for sp=0,player:get("sp") do
				for i=-1,1,2 do
					local damager = player:fireBullet(
						player.x + GLIMPSE_CORE * i,
						player.y,
						(i + 1) * 90,
						GLIMPSE_DISTANCE,
						GLIMPSE_BASE + GLIMPSE_WET * wet_count,
						resources.sprites.hit,
						DAMAGER_BULLET_PIERCE
					):set("climb", sp * CLIMB)
					damager.spriteSpeed = 2
				end
			end
		end
	end
end)

surfer:addCallback("step", function(player)
	local duration = player:get("oceans_call_duration")
	if duration then
		player:set("oceans_call_duration", duration - 1)
	end
	local ability3 = player:control("ability3")
	if player:get("surfing") == 1 then
		local i_wave = findWave(player)
		local jump = player:control("jump") == input.PRESSED
		local cancel = (ability3 == input.PRESSED) or jump or (player:get("activity") > 5)
		if (not i_wave) or cancel then
			exitSurf(player, jump, cancel)
		else
			player:set("pVspeed", -player:get("pGravity1"))
			--player:set("jump_count", player:get("feather"))
			local speed = i_wave:get("hspeed")
			if not player:collidesMap(player.x + speed, player.y) then
				player.x = player.x + speed
			end
		end
	elseif ability3 ~= input.NEUTRAL then
		if  ability3 == input.RELEASED then
			if player:get("block_surf_held") == 1 then
				player:set("block_surf_held", nil)
			end
		elseif (not player:get("block_surf_held")) or (ability3 == input.PRESSED) then
			local i_wave = findWave(player)
			if i_wave then
				enterSurf(player)
			end
		end
	--[[
	else
		if player:get("restore_jumps") then
			player:set("jump_count", player:get("feather") - player:get("restore_jumps"))
			player:set("restore_jumps", nil)
		end
	--]]
	end
end)

callback.register("onGameStart", function()
	local i_depth = graphics.bindDepth(-8, function()
		for _,player in ipairs(misc.players) do
			if (player:getSurvivor() == surfer) and (player:get("oceans_call_duration") or 0) > 0 then
				local r_player = CycloneLib.Rectangle.new(player)
				graphics.drawImage{
					x = r_player.centerx,
					y = r_player.centery,
					image = resources.sprites.oceans_call,
					alpha = math.max(player:get("oceans_call"), 0.1),
				}
			end
		end
	end):set("persistent", 1)
end)

callback.register("onPlayerDeath", function(player)
	if (player:getSurvivor() == surfer) then
		player.sprite = resources.sprites.death_surfer
		player:set("surfer_dead", 1)
	end
end)

callback.register("onDraw", function()
	for _,player in ipairs(misc.players) do
		if player:getSurvivor() == surfer then
			local dead = player:get("surfer_dead")
			if not dead then return nil end
			local sprite = resources.sprites.death_blob
			local _alpha = 0.8
			for i=0,1 do
				graphics.drawImage{
					x = player.x,
					y = player.y + i * math.sin(dead / 16) * WAVE_WAVE / 2, 
					image = sprite,
					subimage = math.clamp(dead * DEATH_SPRITESPEED, 1, 4),
					alpha = _alpha,
				}
				sprite = resources.sprites.death_surfer
				_alpha = 1
			end
			player:set("surfer_dead", dead + 1)
		end
	end
end)

callback.register("preHit", function(damager, hit)
	if (hit:get("oceans_call_duration") or 0) > 0 then
		local reduction = hit:get("oceans_call") or 0
		damager
		:set("old_damage", damager:get("damage"))
		:set("damage", damager:get("damage") * math.clamp(1 - reduction, 0, 1))
		:set("oceans_call", 1)
	elseif damager:get("oceans_call") == 1 then
		damager
		:set("damage", damager:get("old_damage"))
		:set("oceans_call", nil)
	end
end)

callback.register("onHit", function(damager, hit)
	if damager:get("wet") == 1 then
		hit:applyBuff(wet, SPIN_WET)
	end
end)

surfer:addCallback("levelUp", function(player)
	player:survivorLevelUpStats(HP_LEVEL, DAMAGE_LEVEL, REGEN_LEVEL, ARMOR_LEVEL)
end)

surfer:addCallback("scepter", function(player)
	setSkill(player, 5)
end)