-- Surfer - contents/survivors/surfer/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}
resources.sounds = {}
resources.particles = {}

resources.sprites.wet = restre_spriteLoad("wet.png", 1, 4, 4)

resources.sprites.absorb = restre_spriteLoad("absorb.png", 1, 4, 4)

resources.sprites.title = restre_spriteLoad("title.png", 8, 8, 14 - 6)
resources.sprites.loadout = restre_spriteLoad("loadout.png", 12, 0,0)
resources.sprites.skill_icons = restre_spriteLoad("skill_icons.png", 5, 0, 0)

resources.sprites.idle  = restre_spriteLoad("idle.png", 1, 5, 13 - 6)
resources.sprites.walk  = restre_spriteLoad("walk.png", 8, 8, 14 - 6)
resources.sprites.jump  = restre_spriteLoad("jump.png", 1, 5, 13 - 6)
resources.sprites.climb = restre_spriteLoad("climb.png", 4, 4, 7)
resources.sprites.death = restre_spriteLoad("death.png", 2, 8, 10)
resources.sprites.decoy = restre_spriteLoad("decoy.png", 1, 9, 18)

resources.sprites.death_surfer = restre_spriteLoad("death_surfer.png", 4, 8, 32)
resources.sprites.death_blob = restre_spriteLoad("death_blob.png", 4, 8, 32)

resources.sprites.skill_1 = restre_spriteLoad("skill_1.png", 9, 8, 16 - 5)
resources.sprites.skill_2 = restre_spriteLoad("skill_2.png", 14, 16, 8)
resources.sprites.surf = restre_spriteLoad("surf.png", 8, 6, 8)
--resources.sprites.skill_3 = restre_spriteLoad("skill_3.png", 1, 0, 0)
--resources.sprites.skill_4 = restre_spriteLoad("skill_4.png", 1, 0, 0)

resources.sprites.wave = restre_spriteLoad("wave.png", 1, 32, 16)
resources.sprites.wave_mask = restre_spriteLoad("wave_mask.png", 1, 16, 16)

resources.sprites.hit = restre_spriteLoad("hit.png", 2, 5, 4)

resources.sprites.oceans_call = restre_spriteLoad("oceans_call.png", 1, 8, 8)

resources.sounds.spin = restre_soundLoad("spin.ogg")

resources.sounds.absorb = restre_soundLoad("absorb.ogg")

local absorb = ParticleType.new("Absorb")
absorb:shape("ring")
absorb:color(Color.fromRGB(115, 183, 183))
absorb:alpha(0.8, 0.5, 0)
absorb:size(0.05, 0.1, 0, 0)
absorb:life(12, 24)
resources.particles.absorb = absorb

return resources